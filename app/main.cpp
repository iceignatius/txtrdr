#include <uimg/uimgblit.h>
#include <uimg/uimgfile.h>
#include "cmdopt.h"
#include "txtrdr.h"

using namespace std;

static
string ReadInputText(const string &filename)
{
    string text;

    FILE *userfile = nullptr;

    do
    {
        FILE *file = filename.empty() ? stdin : fopen(filename.c_str(), "r");
        if(!file) break;

        while( !feof(file) && !ferror(file) )
        {
            char buf[128] = {0};
            size_t size = fread(buf, 1, sizeof(buf)-1, file);
            if(size)
                text += buf;
        }
    } while(false);

    if(userfile)
        fclose(userfile);

    return text;
}

int main(int argc, char *argv[])
{
    try
    {
        struct cmdopt opts;
        if(ReadCmdOpts(argc, argv, &opts))
            throw runtime_error("Failed to read command options!");

        string text = ReadInputText(opts.input_file);
        if(text.empty())
            throw runtime_error("Failed to read input!");

        TextRenderer rdr;

        for(string &font : opts.font_list)
        {
            if(rdr.AddFont(font))
                throw runtime_error("Failed to load font \"" + font + "\"");
        }

        if(rdr.SetFontSize(opts.font_size))
            throw runtime_error("Failed to set font size!");

        rdr.SetFgColour(opts.fg_colour);
        rdr.SetTabSize(opts.tab_size);

        const Uimg txtimg = rdr.QuickGen(text);

        Uimg baseimg(
            txtimg.Width() + 2 * opts.border_width,
            txtimg.Height() + 2 * opts.border_width);
        if(baseimg.Fill(opts.bg_colour))
            throw runtime_error("Failed to fill colour!");

        if(uimgblit_blend(
            baseimg.cptr(),
            opts.border_width,
            opts.border_width,
            txtimg,
            nullptr,
            UIMG_BLEND_ALPHA))
        {
            throw runtime_error("Failed to blend image!");
        }

        if(uimgfile_save_file(baseimg, opts.output_file.c_str(), 0))
            throw runtime_error("Failed to write output file!");
    }
    catch(exception &e)
    {
        fprintf(stderr, "ERROR: %s\n", e.what());
        return 1;
    }

    return 0;
}
