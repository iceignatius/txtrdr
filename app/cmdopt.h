#ifndef _CMDOPT_H_
#define _CMDPOT_H_

#include <string>
#include <vector>

struct cmdopt
{
    std::vector<std::string> font_list;
    unsigned font_size;
    unsigned border_width;
    uint32_t fg_colour;
    uint32_t bg_colour;
    unsigned tab_size;
    std::string input_file;
    std::string output_file;
};

int ReadCmdOpts(int argc, char *argv[], struct cmdopt *opts);

#endif
