#include <argp.h>
#include <uimg/uimg.h>
#include "cmdopt.h"

#define HIDDEN(c) ( 0x80 | (c) )

static const struct argp_option options[] =
{
    {
        "font",
        'f',
        "file",
        0,
        "Assign a font file to be used."
        " This argument must be presented,"
        " and can be presented multiple times for multi fonts.",
        0
    },
    {
        "font-size",
        's',
        "value",
        0,
        "Set character height in pixels (default=32).",
        0
    },
    {
        "border",
        'b',
        "width",
        0,
        "Set border width in pixels (default=0).",
        0
    },
    {
        "fg-colour",
        HIDDEN('c'),
        "BBGGRRAA",
        0,
        "Set text colour."
        " The colour is described by 8 hexadecimal digits (default=FFFFFFFF).",
        0
    },
    {
        "bg-colour",
        HIDDEN('g'),
        "BBGGRRAA",
        0,
        "Set background colour."
        " The colour is described by 8 hexadecimal digits (default=00000000).",
        0
    },
    {
        "tab-size",
        HIDDEN('t'),
        "value",
        0,
        "Set tab size in count of characters (default=4).",
        0
    },
    {
        "input",
        'i',
        "file",
        0,
        "Assign a file to read text from."
        " If this argument is omitted, then"
        " the text will be read from the standard input.",
        0
    },
    {0}
};

static
uint32_t ParseColourString(const char *str)
{
    unsigned val = strtoul(str, nullptr, 16);
    unsigned b = ( val >> 3*8 ) & 0xFF;
    unsigned g = ( val >> 2*8 ) & 0xFF;
    unsigned r = ( val >> 1*8 ) & 0xFF;
    unsigned a = ( val >> 0*8 ) & 0xFF;
    return UIMG_MAKE_COLOUR(b, g, r, a);
}

static
error_t key_parser(int key, char *arg, struct argp_state *state)
{
    struct cmdopt *opts = static_cast<struct cmdopt*>(state->input);

    switch(key)
    {
    case 'f':
        opts->font_list.push_back(arg);
        return 0;

    case 's':
        opts->font_size = atoi(arg);
        return 0;

    case 'b':
        opts->border_width = atoi(arg);
        return 0;

    case HIDDEN('c'):
        opts->fg_colour = ParseColourString(arg);
        return 0;

    case HIDDEN('g'):
        opts->bg_colour = ParseColourString(arg);
        return 0;

    case HIDDEN('t'):
        opts->tab_size = atoi(arg);
        return 0;

    case 'i':
        if(!opts->input_file.empty())
        {
            fprintf(state->err_stream, "ERROR: Multiple input files are not supported!\n");
            return -1;
        }
        opts->input_file = arg;
        return 0;

    case ARGP_KEY_ARG:
        if(!opts->output_file.empty())
        {
            fprintf(state->err_stream, "ERROR: Multiple output files are not supported!\n");
            return -1;
        }
        opts->output_file = arg;
        return 0;

    case ARGP_KEY_END:
        if(opts->output_file.empty())
        {
            fprintf(state->err_stream, "ERROR: No output file!\n");
            return -1;
        }
        if(opts->font_list.empty())
        {
            fprintf(state->err_stream, "ERROR: No font file!\n");
            return -1;
        }
        return 0;

    case ARGP_KEY_INIT:
    case ARGP_KEY_ARGS:
    case ARGP_KEY_NO_ARGS:
    case ARGP_KEY_SUCCESS:
    case ARGP_KEY_ERROR:
    case ARGP_KEY_FINI:
        return 0;

    default:
        return ARGP_ERR_UNKNOWN;
    }
}

static const struct argp argp =
{
    .options = options,
    .parser = key_parser,
    .args_doc = "OUTPUT_FILE",
    .doc = "Render text to an image file.",
    .children = nullptr,
    .argp_domain = nullptr,
};

int ReadCmdOpts(int argc, char *argv[], struct cmdopt *opts)
{
    opts->font_size = 32;
    opts->border_width = 0;
    opts->fg_colour = 0xFFFFFFFF;
    opts->bg_colour = 0x00000000;
    opts->tab_size = 4;

    return argp_parse(&argp, argc, argv, ARGP_IN_ORDER, nullptr, opts);
}
