#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <setjmp.h>
#include <cmocka.h>
#include <uimg/uimgfile.h>
#include "txtrdr.h"

#ifdef UIMG_USE_SDL
#   include <SDL2/SDL.h>
#endif

#define CANVAS_WIDTH    640
#define CANVAS_HEIGHT   480
#define CANVAS_BORDER   16
#define FONT_SIZE       32

static const struct uimg_rect canvas_rect =
{
    .x = CANVAS_BORDER,
    .y = CANVAS_BORDER,
    .w = CANVAS_WIDTH - 2*CANVAS_BORDER,
    .h = CANVAS_HEIGHT - 2*CANVAS_BORDER,
};

static
long load_compare_image(const char *filename, const uimg_t *t_img)
{
    /*
     * Return:
     * + Positive (including zero): Average value difference per 16 pixels
     * + Negative: Error occurred or properties mismatch
     */
    uimg_t *f_img = NULL;

    long err = -1;
    do
    {
        if( !( f_img = uimg_create(0, 0) ) )
        {
            err = -3;
            break;
        }

        if(uimgfile_load_file(f_img, filename, 0))
        {
            err = -2;
            break;
        }

        if( uimg_get_width(f_img) != uimg_get_width(t_img) ||
            uimg_get_height(f_img) != uimg_get_height(t_img) )
        {
            err = -1;
            break;
        }

        uimg_begin_buf_access(f_img);
        uimg_begin_buf_access(t_img);

        long long diff = 0;
        int w = uimg_get_width(t_img);
        int h = uimg_get_height(t_img);
        for(int y = 0; y < h; ++y)
        {
            const uint32_t *f_row = uimg_get_row_buf_c(f_img, y);
            const uint32_t *t_row = uimg_get_row_buf_c(t_img, y);
            if( !f_row || !t_row )
            {
                diff = LLONG_MAX;
                break;
            }

            for(int x = 0; x < w; ++x)
            {
                diff += abs( (int) UIMG_COLOUR_GET_B(f_row[x]) - (int) UIMG_COLOUR_GET_B(t_row[x]) );
                diff += abs( (int) UIMG_COLOUR_GET_G(f_row[x]) - (int) UIMG_COLOUR_GET_G(t_row[x]) );
                diff += abs( (int) UIMG_COLOUR_GET_R(f_row[x]) - (int) UIMG_COLOUR_GET_R(t_row[x]) );
                diff += abs( (int) UIMG_COLOUR_GET_A(f_row[x]) - (int) UIMG_COLOUR_GET_A(t_row[x]) );
            }
        }

        uimg_end_buf_access(t_img);
        uimg_end_buf_access(f_img);

        diff /= ( w * h * 4 / 16 );
        err = diff;

    } while(false);

    if(f_img)
        uimg_release(f_img);

    return err;
}

static
int setup_test_res(void **state)
{
#ifdef UIMG_USE_SDL
    int err;
    if(( err = SDL_Init(SDL_INIT_VIDEO) ))
        return err;
#endif

    uimg_t *canvas = uimg_create(CANVAS_WIDTH, CANVAS_HEIGHT);
    assert_non_null(canvas);

    txtrdr_t *rdr = test_malloc(sizeof(*rdr));
    assert_non_null(rdr);

    txtrdr_init(rdr, canvas, NULL, 0);
    uimg_release(canvas);

    *state = rdr;
    return 0;
}

static
int teardown_test_res(void **state)
{
    txtrdr_t *rdr = *state;

    txtrdr_destroy(rdr);
    test_free(rdr);

#ifdef UIMG_USE_SDL
    SDL_Quit();
#endif

    return 0;
}

static
uimg_t* gen_veri_image(const uimg_t *txtimg, const struct uimg_rect *rect)
{
    uimg_t *img = uimg_create(CANVAS_WIDTH, CANVAS_HEIGHT);
    assert_non_null(img);

    assert_int_equal(uimgblit_fill(img, UIMG_COLOUR_BLACK, NULL), 0);
    assert_int_equal(uimgblit_fill(img, UIMG_COLOUR_GREEN, &canvas_rect), 0);
    assert_int_equal(uimgblit_fill(img, UIMG_COLOUR_BLUE, rect), 0);
    assert_int_equal(uimgblit_blend(img, 0, 0, txtimg, NULL, UIMG_BLEND_ALPHA), 0);

    return img;
}

static
int verify_test_result(
    const uimg_t *img, const struct uimg_rect *rect, const char *title)
{
    uimg_t *result = NULL;

    int err = 0;
    do
    {
        if( !( result = gen_veri_image(img, rect) ) )
        {
            err = -3;
            break;
        }

        char dumpname[64];
        snprintf(dumpname, sizeof(dumpname), "dump-%s.png", title);
        if(uimgfile_save_file(result, dumpname, 0))
        {
            err = -2;
            break;
        }

        char sampname[64];
        snprintf(sampname, sizeof(sampname), "samples/%s.png", title);

        long diff = load_compare_image(sampname, result);
        if( diff > 128 )
        {
            fprintf(stderr, "Large diff.: title=%s, diff=%ld\n", title, diff);
            err = -1;
            break;
        }

        err = 0;
    } while(false);

    if(result)
        uimg_release(result);

    return err;
}

#define VERIFY_TEST_RESULT(img, rect) \
    verify_test_result(img, rect, __func__)

static
void load_fonts(void **state)
{
    txtrdr_t *rdr = *state;

    assert_int_equal(txtrdr_set_font_size(rdr, FONT_SIZE), 0);
    assert_int_equal(txtrdr_add_fonts(rdr, "*.ttf"), 2);

    // Reload again to ensure the order of fonts
    txtrdr_clear_fonts(rdr);
    assert_int_equal(txtrdr_add_font(rdr, "LiberationSans-Regular.ttf"), 0);
    assert_int_equal(txtrdr_add_font(rdr, "TW-Kai-98_1.ttf"), 0);
}

static
void test_hallo(void **state)
{
    txtrdr_t *rdr = *state;

    struct uimg_rect rect = canvas_rect;
    assert_int_equal(txtrdr_begin_render(rdr, &rect), 0);
    assert_int_equal(txtrdr_push_text(rdr, "Hallo, guten tag!"), 0);
    assert_int_equal(txtrdr_end_render(rdr, &rect), 0);

    assert_int_equal(VERIFY_TEST_RESULT(txtrdr_get_image(rdr), &rect), 0);
}

static
void test_characters(void **state)
{
    txtrdr_t *rdr = *state;

    struct uimg_rect rect = canvas_rect;
    assert_int_equal(txtrdr_begin_render(rdr, &rect), 0);
    assert_int_equal(txtrdr_push_text(rdr, "ABCDEFGHIJKLM\n"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "NOPQRSTUVWXYZ\n"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "abcdefghijklm\n"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "nopqrstuvwxyz\n"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "0123456789\n"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "!\"#$%&'()*+,./:;\n"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "<=>?@[\\]^_`{|}~\n"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "我喜歡閱讀！\n"), 0);
    assert_int_equal(txtrdr_end_render(rdr, &rect), 0);

    assert_int_equal(VERIFY_TEST_RESULT(txtrdr_get_image(rdr), &rect), 0);
}

static
void test_large_font(void **state)
{
    txtrdr_t *rdr = *state;

    unsigned fontsize = txtrdr_get_font_size(rdr);
    assert_int_equal(txtrdr_set_font_size(rdr, fontsize*3), 0);

    struct uimg_rect rect = canvas_rect;
    assert_int_equal(txtrdr_begin_render(rdr, &rect), 0);
    assert_int_equal(txtrdr_push_text(rdr, "Large font"), 0);
    assert_int_equal(txtrdr_end_render(rdr, &rect), 0);

    assert_int_equal(VERIFY_TEST_RESULT(txtrdr_get_image(rdr), &rect), 0);

    assert_int_equal(txtrdr_set_font_size(rdr, fontsize), 0);
}

static
void test_small_font(void **state)
{
    txtrdr_t *rdr = *state;

    unsigned fontsize = txtrdr_get_font_size(rdr);
    assert_int_equal(txtrdr_set_font_size(rdr, fontsize/2), 0);

    struct uimg_rect rect = canvas_rect;
    assert_int_equal(txtrdr_begin_render(rdr, &rect), 0);
    assert_int_equal(txtrdr_push_text(rdr, "small font"), 0);
    assert_int_equal(txtrdr_end_render(rdr, &rect), 0);

    assert_int_equal(VERIFY_TEST_RESULT(txtrdr_get_image(rdr), &rect), 0);

    assert_int_equal(txtrdr_set_font_size(rdr, fontsize), 0);
}

static
void test_spaces(void **state)
{
    txtrdr_t *rdr = *state;

    struct uimg_rect rect = canvas_rect;
    assert_int_equal(txtrdr_begin_render(rdr, &rect), 0);
    assert_int_equal(txtrdr_push_text(rdr, "Text with spaces."), 0);
    assert_int_equal(txtrdr_end_render(rdr, &rect), 0);

    assert_int_equal(VERIFY_TEST_RESULT(txtrdr_get_image(rdr), &rect), 0);
}

static
void test_line_ends(void **state)
{
    txtrdr_t *rdr = *state;

    struct uimg_rect rect = canvas_rect;
    assert_int_equal(txtrdr_begin_render(rdr, &rect), 0);
    assert_int_equal(txtrdr_push_text(rdr, "Text\nwith\nline\nends."), 0);
    assert_int_equal(txtrdr_end_render(rdr, &rect), 0);

    assert_int_equal(VERIFY_TEST_RESULT(txtrdr_get_image(rdr), &rect), 0);
}

static
void test_align_left(void **state)
{
    txtrdr_t *rdr = *state;

    int align = txtrdr_get_align(rdr);
    txtrdr_set_align(rdr, TXTRDR_LEFT);

    struct uimg_rect rect = canvas_rect;
    assert_int_equal(txtrdr_begin_render(rdr, &rect), 0);
    assert_int_equal(txtrdr_push_text(rdr, "Text align left."), 0);
    assert_int_equal(txtrdr_end_render(rdr, &rect), 0);

    assert_int_equal(VERIFY_TEST_RESULT(txtrdr_get_image(rdr), &rect), 0);

    txtrdr_set_align(rdr, align);
}

static
void test_align_right(void **state)
{
    txtrdr_t *rdr = *state;

    int align = txtrdr_get_align(rdr);
    txtrdr_set_align(rdr, TXTRDR_RIGHT);

    struct uimg_rect rect = canvas_rect;
    assert_int_equal(txtrdr_begin_render(rdr, &rect), 0);
    assert_int_equal(txtrdr_push_text(rdr, "Text align right."), 0);
    assert_int_equal(txtrdr_end_render(rdr, &rect), 0);

    assert_int_equal(VERIFY_TEST_RESULT(txtrdr_get_image(rdr), &rect), 0);

    txtrdr_set_align(rdr, align);
}

static
void test_align_centre(void **state)
{
    txtrdr_t *rdr = *state;

    int align = txtrdr_get_align(rdr);
    txtrdr_set_align(rdr, TXTRDR_CENTRE);

    struct uimg_rect rect = canvas_rect;
    assert_int_equal(txtrdr_begin_render(rdr, &rect), 0);
    assert_int_equal(txtrdr_push_text(rdr, "Text align centre."), 0);
    assert_int_equal(txtrdr_end_render(rdr, &rect), 0);

    assert_int_equal(VERIFY_TEST_RESULT(txtrdr_get_image(rdr), &rect), 0);

    txtrdr_set_align(rdr, align);
}

static
void test_tab_as_space(void **state)
{
    txtrdr_t *rdr = *state;

    int tabmod = txtrdr_get_tab_mode(rdr);
    txtrdr_set_tab_mode(rdr, TXTRDR_TAB_AS_SPACE);

    struct uimg_rect rect = canvas_rect;
    assert_int_equal(txtrdr_begin_render(rdr, &rect), 0);
    assert_int_equal(txtrdr_push_text(rdr, "Treat\t"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "Tabs\t"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "as\t"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "spaces.\n"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "\n"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "Text\t"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "with\t"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "Tabs\n"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "and\t"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "multiple\t"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "lines.\n"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "\n"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "Text without tabs."), 0);
    assert_int_equal(txtrdr_end_render(rdr, &rect), 0);

    assert_int_equal(VERIFY_TEST_RESULT(txtrdr_get_image(rdr), &rect), 0);

    txtrdr_set_tab_mode(rdr, tabmod);
}

static
void test_tabstop(void **state)
{
    txtrdr_t *rdr = *state;

    int tabmod = txtrdr_get_tab_mode(rdr);
    txtrdr_set_tab_mode(rdr, TXTRDR_TABSTOP);

    int tabsize = txtrdr_get_tab_size(rdr);
    txtrdr_set_tab_size(rdr, 8);

    struct uimg_rect rect = canvas_rect;
    assert_int_equal(txtrdr_begin_render(rdr, &rect), 0);
    assert_int_equal(txtrdr_push_text(rdr, "Text\t"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "with\t"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "Tab\t"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "stops.\n"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "For\t"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "long-long\t"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "and\t"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "short\t"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "words.\n"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "\n"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "Text\t"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "with\t"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "Tabs\n"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "and\t"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "multiple\t"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "lines.\n"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "\n"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "Text without tabs."), 0);
    assert_int_equal(txtrdr_end_render(rdr, &rect), 0);

    assert_int_equal(VERIFY_TEST_RESULT(txtrdr_get_image(rdr), &rect), 0);

    txtrdr_set_tab_size(rdr, tabsize);
    txtrdr_set_tab_mode(rdr, tabmod);
}

static
void test_tab_left_right(void **state)
{
    txtrdr_t *rdr = *state;

    int tabmod = txtrdr_get_tab_mode(rdr);
    txtrdr_set_tab_mode(rdr, TXTRDR_TAB_LEFT_RIGHT);

    struct uimg_rect rect = canvas_rect;
    assert_int_equal(txtrdr_begin_render(rdr, &rect), 0);
    assert_int_equal(txtrdr_push_text(rdr, "Tabs" "\t" "align" "\n"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "text" "\t" "to" "\n"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "left" "\t" "right" "\n"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "\n"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "Line" "\t"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "with" "\t"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "multiple" "\t"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "Tabs." "\n"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "\n"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "\t" "Single-block-at-right" "\n"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "Single-block-at-left" "\t" "\n"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "\n"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "Text" "\t"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "with" "\t"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "Tabs" "\n"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "and" "\t"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "multiple" "\t"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "lines." "\n"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "\n"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "Text without tabs."), 0);
    assert_int_equal(txtrdr_end_render(rdr, &rect), 0);

    assert_int_equal(VERIFY_TEST_RESULT(txtrdr_get_image(rdr), &rect), 0);

    txtrdr_set_tab_mode(rdr, tabmod);
}

static
void test_tab_columns(void **state)
{
    txtrdr_t *rdr = *state;

    int tabmod = txtrdr_get_tab_mode(rdr);
    txtrdr_set_tab_mode(rdr, TXTRDR_TAB_COLUMNS);

    struct uimg_rect rect = canvas_rect;
    assert_int_equal(txtrdr_begin_render(rdr, &rect), 0);
    assert_int_equal(txtrdr_push_text(rdr, "Tabs" "\t"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "align" "\t"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "text" "\t"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "to" "\t"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "columns." "\n"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "\n"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "\t" "Single-right" "\n"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "Single-left" "\t" "\n"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "\t" "Single-centre" "\t" "\n"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "\n"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "Text" "\t"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "with" "\t"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "Tabs" "\n"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "and" "\t"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "multiple" "\t"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "lines." "\n"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "\n"), 0);
    assert_int_equal(txtrdr_push_text(rdr, "Text without tabs."), 0);
    assert_int_equal(txtrdr_end_render(rdr, &rect), 0);

    assert_int_equal(VERIFY_TEST_RESULT(txtrdr_get_image(rdr), &rect), 0);

    txtrdr_set_tab_mode(rdr, tabmod);
}

static
void test_colours(void **state)
{
    txtrdr_t *rdr = *state;

    uint32_t fg_colour = txtrdr_get_fg_colour(rdr);
    uint32_t bg_colour = txtrdr_get_bg_colour(rdr);
    txtrdr_set_fg_colour(rdr, UIMG_MAKE_COLOUR(128, 128, 255, 255));
    txtrdr_set_bg_colour(rdr, UIMG_MAKE_COLOUR(255, 255, 128, 96));

    struct uimg_rect rect = canvas_rect;
    assert_int_equal(txtrdr_begin_render(rdr, &rect), 0);
    assert_int_equal(txtrdr_push_text(rdr, "Text with colours"), 0);
    assert_int_equal(txtrdr_end_render(rdr, &rect), 0);

    assert_int_equal(VERIFY_TEST_RESULT(txtrdr_get_image(rdr), &rect), 0);

    txtrdr_set_fg_colour(rdr, fg_colour);
    txtrdr_set_bg_colour(rdr, bg_colour);
}

static
void test_quick_gen(void **state)
{
    txtrdr_t rdr;
    txtrdr_init(&rdr, NULL, NULL, 0);

    assert_int_equal(txtrdr_set_font_size(&rdr, FONT_SIZE), 0);
    assert_int_equal(txtrdr_add_font(&rdr, "LiberationSans-Regular.ttf"), 0);
    assert_int_equal(txtrdr_add_font(&rdr, "TW-Kai-98_1.ttf"), 0);

    txtrdr_set_fg_colour(&rdr, UIMG_COLOUR_BLUE);
    txtrdr_set_bg_colour(&rdr, UIMG_COLOUR_GREEN);

    const uimg_t *result = txtrdr_quick_gen(&rdr, "Quick generate" "\n" "with colours");
    assert_non_null(result);
    assert_int_equal(VERIFY_TEST_RESULT(result, NULL), 0);

    txtrdr_destroy(&rdr);
}

int main(int argc, char *argv[])
{
    struct CMUnitTest tests[] =
    {
        cmocka_unit_test(load_fonts),
        cmocka_unit_test(test_hallo),
        cmocka_unit_test(test_characters),
        cmocka_unit_test(test_large_font),
        cmocka_unit_test(test_small_font),
        cmocka_unit_test(test_spaces),
        cmocka_unit_test(test_line_ends),
        cmocka_unit_test(test_align_left),
        cmocka_unit_test(test_align_right),
        cmocka_unit_test(test_align_centre),
        cmocka_unit_test(test_tab_as_space),
        cmocka_unit_test(test_tabstop),
        cmocka_unit_test(test_tab_left_right),
        cmocka_unit_test(test_tab_columns),
        cmocka_unit_test(test_colours),
        cmocka_unit_test(test_quick_gen),
    };

    return cmocka_run_group_tests_name(
        "TXTRDR test", tests, setup_test_res, teardown_test_res);
}
