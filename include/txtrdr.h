/**
 * @file
 * @brief     Text renderer
 * @author    王文佑
 * @date      2022/07/11
 * @copyright ZLib Licence
 */
#ifndef _TXTRDR_H_
#define _TXTRDR_H_

#include <stdbool.h>
#include <uimg/uimgblit.h>
#include "fonts.h"

#ifdef __cplusplus
#   include <string>
#endif

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Align mode
 */
enum txtrdr_align
{
    TXTRDR_LEFT,    ///< Align text to left
    TXTRDR_RIGHT,   ///< Align text to right
    TXTRDR_CENTRE,  ///< Align text to centre
};

/**
 * Tab mode
 */
enum txtrdr_tabmod
{
    TXTRDR_TAB_AS_SPACE,        ///< Treat tab as a space character.
    TXTRDR_TABSTOP,             ///< Tabs in text is just like
                                ///< a normal tab stop with a tab size.

    TXTRDR_TAB_LEFT_RIGHT,      ///< One tab in a text line to separate text
                                ///< and align them to the left and right of the line.
                                ///<
                                ///< @remarks
                                ///< * The alignment mode is ignored in this tab mode.
                                ///< * If there are multiple of tabs in a line,
                                ///<   then the behaviour is undefined!
                                ///< * If the text line is longer than the canvas area,
                                ///<   then the behaviour is undefuned!

    TXTRDR_TAB_COLUMNS,         ///< Tabs in text is used to separate text in
                                ///< multiple of columns.
                                ///<
                                ///< @remarks
                                ///< * If the text line is longer than the canvas area,
                                ///<   then the behaviour is undefuned!
};

/**
 * @class txtrdr
 * @brief Text renderer
 */
typedef struct txtrdr
{
    // WARNING: All variables are private!

    uimg_t *canvas;
    bool use_user_canvas;
    struct txtrdr_fonts *fonts;
    unsigned fonts_cache_num;

    uint32_t fg_colour;
    uint32_t bg_colour;
    int align;
    int tabsize;
    int tabmode;

    int space_width;
    struct list_head *glyphs;

    struct uimg_rect text_rect;
    struct uimg_rect draw_rect;

} txtrdr_t;

void txtrdr_init(
    txtrdr_t *self, uimg_t *canvas, const txtrdr_fonts_t *fonts, unsigned cache_num);
void txtrdr_destroy(txtrdr_t *self);

const txtrdr_fonts_t* txtrdr_get_fonts(const txtrdr_t *self);
int txtrdr_add_font(txtrdr_t *self, const char *filename);
unsigned txtrdr_add_fonts(txtrdr_t *self, const char *wildcard);
void txtrdr_clear_fonts(txtrdr_t *self);
int txtrdr_set_font_size(txtrdr_t *self, unsigned height);
unsigned txtrdr_get_font_size(const txtrdr_t *self);

void txtrdr_set_fg_colour(txtrdr_t *self, uint32_t colour);
void txtrdr_set_bg_colour(txtrdr_t *self, uint32_t colour);
void txtrdr_set_align(txtrdr_t *self, int align);
void txtrdr_set_tab_size(txtrdr_t *self, int tabsize);
void txtrdr_set_tab_mode(txtrdr_t *self, int tabmod);

uint32_t txtrdr_get_fg_colour(const txtrdr_t *self);
uint32_t txtrdr_get_bg_colour(const txtrdr_t *self);
int txtrdr_get_align(const txtrdr_t *self);
int txtrdr_get_tab_size(const txtrdr_t *self);
int txtrdr_get_tab_mode(const txtrdr_t *self);

int txtrdr_begin_render(txtrdr_t *self, const struct uimg_rect *rect);
int txtrdr_end_render(txtrdr_t *self, struct uimg_rect *rect);
bool txtrdr_in_rendering(const txtrdr_t *self);
int txtrdr_push_text(txtrdr_t *self, const char *text);

const uimg_t* txtrdr_get_image(const txtrdr_t *self);
const uimg_t* txtrdr_quick_gen(txtrdr_t *self, const char *text);

#ifdef __cplusplus
}  // extern "C"
#endif

#ifdef __cplusplus

/**
 * TXTRDR encapsulate class
 */
class TextRenderer : protected txtrdr_t
{
public:
    /// Constructor
    TextRenderer(
        Uimg *canvas = nullptr,
        const txtrdr_fonts_t *fonts = nullptr,
        unsigned cache_num = 0)
    {
        txtrdr_init(this, canvas ? canvas->cptr() : nullptr, fonts, cache_num);
    }

    /// Destructor
    ~TextRenderer()
    {
        txtrdr_destroy(this);
    }

    TextRenderer(const TextRenderer &src) = delete;
    TextRenderer& operator=(const TextRenderer &src) = delete;

public:
    /// @see txtrdr_get_fonts()
    const txtrdr_fonts_t* Fonts() const { return txtrdr_get_fonts(this); }
    /// @see txtrdr_add_font()
    int AddFont(const std::string &filename) { return txtrdr_add_font(this, filename.c_str()); }
    /// @see txtrdr_add_fonts()
    unsigned AddFonts(const std::string &wildcard) { return txtrdr_add_fonts(this, wildcard.c_str()); }
    /// @see txtrdr_clear_fonts()
    void ClearFonts() { txtrdr_clear_fonts(this); }
    /// @see txtrdr_set_font_size()
    int SetFontSize(unsigned height) { return txtrdr_set_font_size(this, height); }
    /// @see txtrdr_get_font_size()
    unsigned FontSize() const { return txtrdr_get_font_size(this); }

    /// @see txtrdr_set_fg_colour()
    void SetFgColour(uint32_t colour) { txtrdr_set_fg_colour(this, colour); }
    /// @see txtrdr_set_bg_colour()
    void SetBgColour(uint32_t colour) { txtrdr_set_bg_colour(this, colour); }
    /// @see txtrdr_set_align()
    void SetAlign(int align) { txtrdr_set_align(this, align); }
    /// @see txtrdr_set_tab_size()
    void SetTabSize(int tabsize) { txtrdr_set_tab_size(this, tabsize); }
    /// @see txtrdr_set_tab_mode()
    void SetTabMode(int tabmod) { txtrdr_set_tab_mode(this, tabmod); }

    /// @see txtrdr_get_fg_colour()
    uint32_t FgColour() const { return txtrdr_get_fg_colour(this); }
    /// @see txtrdr_get_bg_colour()
    uint32_t BgColour() const { return txtrdr_get_bg_colour(this); }
    /// @see txtrdr_get_align()
    int Align() const { return txtrdr_get_align(this); }
    /// @see txtrdr_get_tab_size()
    int TabSize() const { return txtrdr_get_tab_size(this); }
    /// @see txtrdr_get_tab_mode()
    int TabMode() const { return txtrdr_get_tab_mode(this); }

    /// @see txtrdr_begin_render()
    int BeginRender(const struct uimg_rect *rect) { return txtrdr_begin_render(this, rect); }
    /// @see txtrdr_end_render()
    int EndRender(struct uimg_rect *rect) { return txtrdr_end_render(this, rect); }
    /// @see txtrdr_in_rendering()
    bool InRendering() const { return txtrdr_in_rendering(this); }
    /// @see txtrdr_push_text()
    int PushText(const std::string &text) { return txtrdr_push_text(this, text.c_str()); }

    /// @see txtrdr_get_image()
    const Uimg Image() const
    {
        return uimg_shadow(txtrdr_get_image(this));
    }

    /// @see txtrdr_quick_gen()
    const Uimg QuickGen(const std::string &text)
    {
        return uimg_shadow(txtrdr_quick_gen(this, text.c_str()));
    }
};

#endif  // __cplusplus

#endif
