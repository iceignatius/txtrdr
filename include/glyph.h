#ifndef _TXTRDR_GLYPH_H_
#define _TXTRDR_GLYPH_H_

#include <uimg/uimgblit.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Font glyph type
 */
enum txtrdr_glyph_type
{
    TXTRDR_GLYPH_NORMAL,        ///< The glyph is a normal printable glyph

    TXTRDR_GLYPH_UNREADABLE,    ///< This glyph is a fallback glyph
                                ///< for unreadable code point

    TXTRDR_GLYPH_INVISIBLE,     ///< This code point is invisible
};

/**
 * Font glyph
 */
struct txtrdr_glyph
{
    unsigned long long code;    ///< Number of the character code point.
    int type;                   ///< Glyph type defined in ::txtrdr_glyph_type.

    int height;         ///< Height of metrics of the character.
    int advance;        ///< Width of metrics of the character.
    int bearing_x;      ///< Distance from the metrics origin to
                        ///< the left of image in pixels.
    int bearing_y;      ///< Distance from baseline to
                        ///< the top of image in pixels.
    uint32_t colour;    ///< Colour of the glyph image.

    uimg_t *image;          ///< Glyph image in grey scale.
    struct uimg_rect area;  ///< Area on image that have the glyph.
};

struct txtrdr_glyph* txtrdr_glyph_shadow(const struct txtrdr_glyph *glyph);
void txtrdr_glyph_release(struct txtrdr_glyph *glyph);

#ifdef __cplusplus
}   // extern "C"
#endif

#endif
