#ifndef _TXTRDR_FONTS_H_
#define _TXTRDR_FONTS_H_

#include <stdint.h>
#include "glyph.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct txtrdr_fonts txtrdr_fonts_t;

txtrdr_fonts_t* txtrdr_fonts_create(unsigned cache_num);
txtrdr_fonts_t* txtrdr_fonts_shadow(const txtrdr_fonts_t *self);
void txtrdr_fonts_release(txtrdr_fonts_t *self);

unsigned txtrdr_fonts_get_cache_num(const txtrdr_fonts_t *self);

int txtrdr_fonts_add_font(txtrdr_fonts_t *self, const char *filename);
void txtrdr_fonts_clear(txtrdr_fonts_t *self);

int txtrdr_fonts_set_size(txtrdr_fonts_t *self, unsigned height);
unsigned txtrdr_fonts_get_size(const txtrdr_fonts_t *self);
unsigned txtrdr_fonts_get_ascender(const txtrdr_fonts_t *self);

const struct txtrdr_glyph* txtrdr_fonts_get_glyph(
    txtrdr_fonts_t *self, unsigned long long code, uint32_t colour);

#ifdef __cplusplus
}  // extern "C"
#endif

#endif
