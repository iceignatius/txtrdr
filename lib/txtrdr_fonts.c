#include <assert.h>
#include <ctype.h>
#include <limits.h>
#include <stdatomic.h>
#include <stdlib.h>
#include "txtrdr_ttfont.h"
#include "txtrdr_gpool.h"
#include "fonts.h"

struct txtrdr_fonts
{
    atomic_int refcnt;

    unsigned font_size;
    struct list_head font_list;
    pthread_mutex_t font_mutex;

    txtrdr_gpool_t gpool;
    unsigned cache_num;
};

static
void txtrdr_fonts_init(txtrdr_fonts_t *self, unsigned cache_num)
{
    atomic_init(&self->refcnt, 1);

    self->font_size = 12;
    INIT_LIST_HEAD(&self->font_list);

    int err = pthread_mutex_init(&self->font_mutex, NULL);
    assert( err == 0 );
    (void) err; // Avoid the unused warning!

    txtrdr_gpool_init(&self->gpool);
    self->cache_num = cache_num;
}

static
void txtrdr_fonts_destroy(txtrdr_fonts_t *self)
{
    txtrdr_fonts_clear(self);

    txtrdr_gpool_destroy(&self->gpool);
    pthread_mutex_destroy(&self->font_mutex);
}

txtrdr_fonts_t* txtrdr_fonts_create(unsigned cache_num)
{
    txtrdr_fonts_t *inst = malloc(sizeof(*inst));

    if(inst)
        txtrdr_fonts_init(inst, cache_num);

    return inst;
}

txtrdr_fonts_t* txtrdr_fonts_shadow(const txtrdr_fonts_t *self)
{
    txtrdr_fonts_t *inst = (txtrdr_fonts_t*) self;

    if(inst)
        atomic_fetch_add(&inst->refcnt, 1);

    return inst;
}

void txtrdr_fonts_release(txtrdr_fonts_t *self)
{
    if( self && 1 == atomic_fetch_sub(&self->refcnt, 1) )
    {
        txtrdr_fonts_destroy(self);
        free(self);
    }
}

unsigned txtrdr_fonts_get_cache_num(const txtrdr_fonts_t *self)
{
    return self->cache_num;
}

int txtrdr_fonts_add_font(txtrdr_fonts_t *self, const char *filename)
{
    pthread_mutex_lock(&self->font_mutex);

    txtrdr_ttfont_t *font = txtrdr_ttfont_create_open(filename, self->font_size);
    if(font)
        list_add_tail(&font->fontnode, &self->font_list);

    pthread_mutex_unlock(&self->font_mutex);

    return font ? 0 : -1;
}

void txtrdr_fonts_clear(txtrdr_fonts_t *self)
{
    pthread_mutex_lock(&self->font_mutex);

    txtrdr_ttfont_t *font, *tmp;
    list_for_each_entry_safe(font, tmp, &self->font_list, fontnode)
    {
        txtrdr_ttfont_release(font);
    }

    INIT_LIST_HEAD(&self->font_list);

    pthread_mutex_unlock(&self->font_mutex);
}

int txtrdr_fonts_set_size(txtrdr_fonts_t *self, unsigned height)
{
    if( self->font_size == height )
        return 0;

    int err = 0;

    pthread_mutex_lock(&self->font_mutex);

    self->font_size = height;

    txtrdr_ttfont_t *font;
    list_for_each_entry(font, &self->font_list, fontnode)
    {
        int rc = txtrdr_ttfont_set_font_size(font, self->font_size);
        err = err ? err : rc;
    }

    pthread_mutex_unlock(&self->font_mutex);

    return err;
}

unsigned txtrdr_fonts_get_size(const txtrdr_fonts_t *self)
{
    return self->font_size;
}

unsigned txtrdr_fonts_get_ascender(const txtrdr_fonts_t *self)
{
    pthread_mutex_lock(&((txtrdr_fonts_t*)self)->font_mutex);

    txtrdr_ttfont_t *font =
        list_first_entry_or_null(&self->font_list, txtrdr_ttfont_t, fontnode);
    unsigned ascender = font ? txtrdr_ttfont_get_font_ascender(font) : 0;

    pthread_mutex_unlock(&((txtrdr_fonts_t*)self)->font_mutex);

    return ascender;
}

static
int load_gryph_from_fonts(
    struct list_head *font_list,
    unsigned long long code,
    uint32_t colour,
    struct txtrdr_glyph *glyph)
{
    int err = -1;

    txtrdr_ttfont_t *font;
    list_for_each_entry(font, font_list, fontnode)
    {
        if( !( err = txtrdr_ttfont_get_glyph(font, code, colour, glyph) ) )
            break;
    }

    return err;
}

static
void fill_ctrl_glyph_info(
    struct txtrdr_glyph *glyph, unsigned long long code, int height, uint32_t colour)
{
    glyph->code = code;
    glyph->type = TXTRDR_GLYPH_INVISIBLE;

    glyph->height = height;
    glyph->advance = 0;
    glyph->bearing_x = 0;
    glyph->bearing_y = 0;
    glyph->colour = colour;

    glyph->image = NULL;
    glyph->area.x = 0;
    glyph->area.y = 0;
    glyph->area.w = 0;
    glyph->area.h = 0;
}

static
void fill_square_glyph_info(
    struct txtrdr_glyph *glyph,
    unsigned long long code,
    int height,
    uint32_t colour,
    const uimg_t *image)
{
    glyph->code = code;
    glyph->type = TXTRDR_GLYPH_UNREADABLE;

    glyph->height = height;
    glyph->advance = uimg_get_width(image);
    glyph->bearing_x = 0;
    glyph->bearing_y = 0;
    glyph->colour = colour;

    glyph->image = uimg_shadow(image);
    glyph->area.x = 0;
    glyph->area.y = 0;
    glyph->area.w = uimg_get_width(image);
    glyph->area.h = uimg_get_height(image);
}

static
int draw_unreadable_square(uimg_t *image, uint32_t colour)
{
    unsigned w = uimg_get_width(image);
    unsigned h = uimg_get_height(image);

    static const unsigned border_width = 2;
    if( w <= 4*border_width || h <= 4*border_width )
    {
        uimg_fill(image, colour);
        return 0;
    }

    int err;
    if(( err = uimg_fill(image, 0) ))
        return err;

    struct uimg_rect rect;
    rect.x = border_width;
    rect.y = border_width;
    rect.w = w - 2*border_width;
    rect.h = h - 2*border_width;
    if(( err = uimgblit_fill(image, colour, &rect) ))
        return err;

    rect.x = 2*border_width;
    rect.y = 2*border_width;
    rect.w = w - 4*border_width;
    rect.h = h - 4*border_width;
    if(( err = uimgblit_fill(image, 0, &rect) ))
        return err;

    return 0;
}

static
int gen_square_glyph(
    unsigned width,
    unsigned height,
    unsigned long long code,
    uint32_t colour,
    struct txtrdr_glyph *glyph)
{
    uimg_t *image = NULL;

    int succ = false;
    do
    {
        if( !( image = uimg_create(width, height) ) )
            break;

        if(draw_unreadable_square(image, colour))
            break;

        fill_square_glyph_info(glyph, code, height, colour, image);

        succ = true;
    } while(false);

    if(image)
        uimg_release(image);

    return succ ? 0 : -1;
}

const struct txtrdr_glyph* txtrdr_fonts_get_glyph(
    txtrdr_fonts_t *self, unsigned long long code, uint32_t colour)
{
    const struct txtrdr_glyph *result = NULL;

    struct txtrdr_glyph glyph = {0};

    pthread_mutex_lock(&self->font_mutex);

    do
    {
        if( !txtrdr_gpool_get_cache_num(&self->gpool) &&
            txtrdr_gpool_setup(&self->gpool, self->cache_num) )
        {
            break;
        }

        if(( result = txtrdr_gpool_find(&self->gpool, code, self->font_size, colour) ))
            break;  // Success

        int err = 0;
        if( code < CHAR_MAX && iscntrl(code) )
            fill_ctrl_glyph_info(&glyph, code, self->font_size, colour);
        else
            err = load_gryph_from_fonts(&self->font_list, code, colour, &glyph);

        if(err)
            err = gen_square_glyph(
                self->font_size >> 1, self->font_size, code, colour, &glyph);
        if(err)
            break;

        result = txtrdr_gpool_insert(&self->gpool, &glyph);
    } while(false);

    pthread_mutex_unlock(&self->font_mutex);

    if(glyph.image)
        uimg_release(glyph.image);

    return result;
}
