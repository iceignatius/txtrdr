#include <stddef.h>
#include <stdint.h>
#include "txtrdr_utf8.h"

static
unsigned count_leading_ones(uint8_t x)
{
    x = ~x;
    x |= x >> 1;
    x |= x >> 2;
    x |= x >> 4;
    x = ~x;

    x = ( x & 0x55 ) + ( ( x >> 1 ) & 0x55 );
    x = ( x & 0x33 ) + ( ( x >> 2 ) & 0x33 );
    x = ( x & 0x0F ) + ( ( x >> 4 ) & 0x0F );

    return x;
}

const char* txtrdr_utf8_decode(const char *str, unsigned long long *code)
{
    if( !str || !str[0] ) return NULL;

    unsigned len = count_leading_ones(str[0]);
    if( len < 2 || 6 < len )
        len = 1;

    uint8_t head_mask = 0xFF >> len;
    unsigned long long val = str[0] & head_mask;
    ++str;

    for(; --len && str[0]; ++str)
    {
        val <<= 6;
        val |= str[0] & 0x3F;
    }

    *code = val;
    return str;
}
