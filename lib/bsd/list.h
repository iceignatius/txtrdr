/*-
 * Copyright (c) 2010 Isilon Systems, Inc.
 * Copyright (c) 2010 iX Systems, Inc.
 * Copyright (c) 2010 Panasas, Inc.
 * Copyright (c) 2013-2016 Mellanox Technologies, Ltd.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice unmodified, this list of conditions, and the following
 *    disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $FreeBSD$
 */
#ifndef _USR_LIST_H_
#define _USR_LIST_H_

/**
 * Definition of list node, and also the list
 */
struct list_head {
	struct list_head *next;
	struct list_head *prev;
};

/**
 * Initialise a list on declaration
 */
#define LIST_HEAD_INIT(name) { &(name), &(name) }

#define DEFINE_LIST_HEAD(name) \
	struct list_head name = LIST_HEAD_INIT(name)
#define LIST_HEAD(name)	\
	struct list_head name = LIST_HEAD_INIT(name)

/**
 * Initialise a list
 */
static inline
void INIT_LIST_HEAD(struct list_head *list)
{
	list->next = list->prev = list;
}

/**
 * Check if a list is empty
 *
 * @return TRUE if it is empty, and FALSE if not.
 */
static inline
int list_empty(const struct list_head *head)
{
	return head->next == head;
}

/**
 * Similarly to list_empty() but with more checks
 */
static inline
int list_empty_careful(const struct list_head *head)
{
	struct list_head *next = head->next;

	return ((next == head) && (next == head->prev));
}

/**
 * Check if list have one node only
 *
 * @return TRUE if it have one node only, and FALSE if not.
 */
static inline
int list_is_singular(const struct list_head *head)
{
	return !list_empty(head) && (head->next == head->prev);
}

/**
 * Check if a node is the first node of a list
 *
 * @param list The node to be checked
 * @param head the list
 * @return TRUE if it is the first node, and FALSE if not.
 */
static inline
int list_is_first(const struct list_head *list, const struct list_head *head)
{
	return list->prev == head;
}

/**
 * Check if a node is the last node of a list
 *
 * @param list The node to be checked
 * @param head the list
 * @return TRUE if it is the last node, and FALSE if not.
 */
static inline
int list_is_last(const struct list_head *list, const struct list_head *head)
{
	return list->next == head;
}

/*
 * Link a new entry between two known consecutive entries
 *
 * This is only for internal list manipulation where we know
 * the prev/next entries already!
 */
static inline
void __list_add(struct list_head *newe,
	struct list_head *prev,
	struct list_head *next)
{
	next->prev = newe;
	newe->next = next;
	newe->prev = prev;
	prev->next = newe;
}

/**
 * Link a node to the list head
 *
 * @param newe The node to be added
 * @param head The list to be appended
 */
static inline
void list_add(struct list_head *newe, struct list_head *head)
{
	__list_add(newe, head, head->next);
}

/**
 * Link a node to the list tail
 *
 * @param newe The node to be added
 * @param head The list to be appended
 */
static inline
void list_add_tail(struct list_head *newe, struct list_head *head)
{
	__list_add(newe, head->prev, head);
}

/*
 * Unlink a list entry by making the prev/next entries
 * point to each other.
 *
 * This is only for internal list manipulation where we know
 * the prev/next entries already!
 */
static inline
void __list_del(struct list_head *prev, struct list_head *next)
{
	next->prev = prev;
	prev->next = next;
}

/**
 * Unlink entry from list
 *
 * @param entry The node to be unlink from the list
 *
 * @note
 * list_empty() on entry does not return true after this,
 * the entry is in an undefined state.
 */
static inline
void list_del(struct list_head *entry)
{
	__list_del(entry->prev, entry->next);
}

/**
 * Unlink a node from list and reinitialize it
 *
 * @param entry The node to be unlink
 */
static inline
void list_del_init(struct list_head *entry)
{
	list_del(entry);
	INIT_LIST_HEAD(entry);
}

/**
 * Unlink from one list and add as another's head
 */
static inline
void list_move(struct list_head *entry, struct list_head *head)
{
	list_del(entry);
	list_add(entry, head);
}

/**
 * Delete from one list and add as another's tail
 */
static inline
void list_move_tail(struct list_head *entry, struct list_head *head)
{
	list_del(entry);
	list_add_tail(entry, head);
}

/**
 * Extract nodes (from first to last) from its parent, and then
 * append them to the tail of another list.
 *
 * @param head The new list to be appended
 * @param first The first node to be extracted
 * @param last The last node to be extracted
 */
static inline
void list_bulk_move_tail(struct list_head *head,
	struct list_head *first,
	struct list_head *last)
{
	first->prev->next = last->next;
	last->next->prev = first->prev;
	head->prev->next = first;
	first->prev = head->prev;
	last->next = head;
	head->prev = last;
}

/**
 * Replace a list node
 *
 * @param old   The node to be unlinked
 * @param newe  The node to be linked to replace the @p old node
 */
static inline
void list_replace(struct list_head *old, struct list_head *newe)
{
	newe->next = old->next;
	newe->next->prev = newe;
	newe->prev = old->prev;
	newe->prev->next = newe;
}

/**
 * Similarly to list_replace() with reinitialise the extracted node
 */
static inline void
list_replace_init(struct list_head *old, struct list_head *newe)
{
	list_replace(old, newe);
	INIT_LIST_HEAD(old);
}

/*
 * Insert a list (@p list) into another (insert between @p prev and @p next)
 *
 * This is only for internal list manipulation,
 * the @p list state will not change and may not reliable after operation.
 */
static inline
void __list_splice(const struct list_head *list,
	struct list_head *prev,
	struct list_head *next)
{
	struct list_head *first;
	struct list_head *last;

	if (list_empty(list))
		return;

	first = list->next;
	last = list->prev;
	first->prev = prev;
	prev->next = first;
	last->next = next;
	next->prev = last;
}

/**
 * Insert a list to another list's head
 *
 * @param list The list to be inserted
 * @param head The list to be appended
 *
 * @note The @p list may not be usable and not reliable after operation
 */
static inline
void list_splice(const struct list_head *list, struct list_head *head)
{
	__list_splice(list, head, head->next);
}

/**
 * Insert a list to another list's tail
 *
 * @param list The list to be inserted
 * @param head The list to be appended
 *
 * @note The @p list may not be usable and not reliable after operation
 */
static inline
void list_splice_tail(struct list_head *list, struct list_head *head)
{
	__list_splice(list, head->prev, head);
}

/**
 * Insert a list to another list's head
 *
 * @param list The list to be extract then insert to another
 * @param head The list to be appended
 *
 * @note The @p list will be reinitialised
 */
static inline
void list_splice_init(struct list_head *list, struct list_head *head)
{
	__list_splice(list, head, head->next);
	INIT_LIST_HEAD(list);
}

/**
 * Insert a list to another list's tail
 *
 * @param list The list to be extract then insert to another
 * @param head The list to be appended
 *
 * @note The @p list will be reinitialised
 */
static inline
void list_splice_tail_init(struct list_head *list, struct list_head *head)
{
	__list_splice(list, head->prev, head);
	INIT_LIST_HEAD(list);
}

static inline
void __list_cut_position(struct list_head *list,
	struct list_head *head,
	struct list_head *entry)
{
	struct list_head *new_first = entry->next;
	list->next = head->next;
	list->next->prev = list;
	list->prev = entry;
	entry->next = list;
	head->next = new_first;
	new_first->prev = head;
}

static inline
void list_cut_position(struct list_head *list,
	struct list_head *head,
	struct list_head *entry)
{
	if (list_empty(head))
		return;
	if (list_is_singular(head) &&
		(head->next != entry && head != entry))
		return;
	if (entry == head)
		INIT_LIST_HEAD(list);
	else
		__list_cut_position(list, head, entry);
}

/**
 * Get the user defined list entry structure
 *
 * @param ptr   The node pointer
 * @param type  The user defined type of the structure this is embedded in
 * @param field Name of the list node field within the user structure
 * @return Pointer of the corresponding user defined structure
 */
#define list_entry(ptr, type, field) \
	((type *)((char *)(ptr)-(char *)(&((type *)0)->field)))

/**
 * Get the first entry of a list
 *
 * @param ptr       The list
 * @param type      The user defined type of the structure this is embedded in
 * @param field     Name of the list node field within the structure
 * @return The first entry
 *
 * @note If the list is empty, then the behaviour is undefined.
 */
#define list_first_entry(ptr, type, field) \
	list_entry((ptr)->next, type, field)

/**
 * Get the last entry of a list
 *
 * @param ptr       The list
 * @param type      The user defined type of the structure this is embedded in
 * @param field     Name of the list node field within the structure
 * @return The last entry
 *
 * @note If the list is empty, then the behaviour is undefined.
 */
#define list_last_entry(ptr, type, field) \
	list_entry((ptr)->prev, type, field)

/**
 * Get the first entry of a list
 *
 * @param ptr       The list
 * @param type      The user defined type of the structure this is embedded in
 * @param field     Name of the list node field within the structure
 * @return The first entry, or NULL of list is empty.
 */
#define list_first_entry_or_null(ptr, type, field) \
	(!list_empty(ptr) ? list_first_entry(ptr, type, field) : NULL)

/**
 * Get the last entry of a list
 *
 * @param ptr       The list
 * @param type      The user defined type of the structure this is embedded in
 * @param field     Name of the list node field within the structure
 * @return The last entry, or NULL of list is empty.
 */
#define list_last_entry_or_null(ptr, type, field) \
	(!list_empty(ptr) ? list_last_entry(ptr, type, field) : NULL)

/**
 * Get the next entry
 *
 * @param ptr       The current entry
 * @param field     Name of the list node field within the structure
 * @return The next entry
 *
 * @note If the current entry is the last one, then the behaviour is undefined.
 */
#define list_next_entry(ptr, field)					\
	list_entry(((ptr)->field.next), typeof(*(ptr)), field)

/**
 * Similarly to list_next_entry() but not change the current entry pointer
 */
#define list_safe_reset_next(ptr, n, field) \
	(n) = list_next_entry(ptr, field)

/**
 * Get the previous entry
 *
 * @param ptr       The current entry
 * @param field     Name of the list node field within the structure
 * @return The previous entry
 *
 * @note If the current entry is the first one, then the behaviour is undefined.
 */
#define list_prev_entry(ptr, field)					\
	list_entry(((ptr)->field.prev), typeof(*(ptr)), field)

/**
 * Iterate over a list
 *
 * @param pos   The list node pointer to be used as a loop counter
 * @param head  The list head to be traveled
 */
#define list_for_each(pos, head) \
	for (pos = (head)->next; pos != (head); pos = (pos)->next)

/**
 * Iterate over a list safe against removal of list entry
 *
 * @param pos   The list node pointer to be used as a loop counter
 * @param n     Another list node pointer to be used as temporary storage
 * @param head  The list head to be traveled
 */
#define list_for_each_safe(pos, n, head) \
	for (pos = (head)->next, n = (pos)->next; \
		pos != (head); \
		pos = n, n = (pos)->next)

/**
 * Iterate over list of entry type
 *
 * @param pos   Pointer of the user defined entry type to be used as a loop counter
 * @param head  The list head to be traveled
 * @param field Name of the list node field within the structure
 */
#define list_for_each_entry(pos, head, field)				\
	for (pos = list_entry((head)->next, typeof(*pos), field); \
		&(pos)->field != (head); \
		pos = list_entry((pos)->field.next, typeof(*pos), field))

/**
 * Iterate over list of entry type
 * safe against removal of list entry
 *
 * @param pos   Pointer of the user defined entry type to be used as a loop counter
 * @param n     Another entry pointer to be used as temporary storage
 * @param head  The list head to be traveled
 * @param field Name of the list node field within the structure
 */
#define list_for_each_entry_safe(pos, n, head, field) \
	for (pos = list_entry((head)->next, typeof(*pos), field), \
		n = list_entry((pos)->field.next, typeof(*pos), field); \
		&(pos)->field != (head); \
		pos = n, \
		n = list_entry(n->field.next, typeof(*n), field))

/**
 * Iterate over list of entry type
 * from a specific position
 *
 * @param pos   Pointer of the user defined entry type to be used as a loop counter
 *              and also the start position
 * @param head  The list head to be traveled
 * @param field Name of the list node field within the structure
 */
#define	list_for_each_entry_from(pos, head, field) \
	for ( ; &(pos)->field != (head); \
		pos = list_entry((pos)->field.next, typeof(*pos), field))

/**
 * similarly to list_for_each_entry_from() but skip the specific start position
 */
#define	list_for_each_entry_continue(pos, head, field) \
	for (pos = list_next_entry((pos), field); \
		&(pos)->field != (head); \
		pos = list_next_entry((pos), field))

#define	list_for_each_entry_safe_from(pos, n, head, field) \
	for (n = list_entry((pos)->field.next, typeof(*pos), field); \
	     &(pos)->field != (head); \
	     pos = n, n = list_entry(n->field.next, typeof(*n), field))

#define list_for_each_entry_reverse(pos, head, field) \
	for (pos = list_entry((head)->prev, typeof(*pos), field); \
		&(pos)->field != (head); \
		pos = list_entry((pos)->field.prev, typeof(*pos), field))

#define	list_for_each_entry_safe_reverse(pos, n, head, field) \
	for (pos = list_entry((head)->prev, typeof(*pos), field), \
		n = list_entry((pos)->field.prev, typeof(*pos), field); \
		&(pos)->field != (head); \
		pos = n, n = list_entry(n->field.prev, typeof(*n), field))

#define	list_for_each_entry_continue_reverse(pos, head, field) \
	for (pos = list_entry((pos)->field.prev, typeof(*pos), field); \
		&(pos)->field != (head); \
		pos = list_entry((pos)->field.prev, typeof(*pos), field))

#define	list_for_each_prev(pos, head) \
	for (pos = (head)->prev; pos != (head); pos = (pos)->prev)

#define	list_for_each_entry_from_reverse(pos, head, field) \
	for (; &pos->field != (head); \
		pos = list_prev_entry(pos, field))

#endif	/* _USR_LIST_H_ */
