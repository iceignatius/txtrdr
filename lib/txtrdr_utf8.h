#ifndef _TXTRDR_UTF8_H_
#define _TXTRDR_UTF8_H_

#ifdef __cplusplus
extern "C" {
#endif

const char* txtrdr_utf8_decode(const char *str, unsigned long long *code);

#ifdef __cplusplus
}   // extern "C"
#endif

#endif
