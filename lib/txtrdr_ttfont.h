#ifndef _TXTRDR_TTFONT_H_
#define _TXTRDR_TTFONT_H_

#include <stdbool.h>
#include "bsd/list.h"
#include "glyph.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct txtrdr_ttfont
{
    struct list_head fontnode;   // Reserved for fonts manager use

    bool lib_ref_added;
    struct FT_FaceRec_ *face;

    unsigned font_height;
    unsigned ascender;
    unsigned descender;
} txtrdr_ttfont_t;

txtrdr_ttfont_t* txtrdr_ttfont_create_open(const char *filename, unsigned height);
void txtrdr_ttfont_release(txtrdr_ttfont_t *self);

int txtrdr_ttfont_set_font_size(txtrdr_ttfont_t *self, unsigned height);
unsigned txtrdr_ttfont_get_font_size(txtrdr_ttfont_t *self);
unsigned txtrdr_ttfont_get_font_ascender(txtrdr_ttfont_t *self);

int txtrdr_ttfont_get_glyph(
    txtrdr_ttfont_t *self,
    unsigned long long code,
    uint32_t colour,
    struct txtrdr_glyph *glyph);

#ifdef __cplusplus
}   // extern "C"
#endif

#endif
