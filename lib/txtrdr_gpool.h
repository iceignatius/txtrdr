#ifndef _TXTRDR_GLYPH_POOL_H_
#define _TXTRDR_GLYPH_POOL_H_

#include <pthread.h>
#include "bsd/list.h"
#include "bsd/tree.h"
#include "glyph.h"

#ifdef __cplusplus
extern "C" {
#endif

RB_HEAD(txtrdr_glyph_map, glyphslot);

typedef struct txtrdr_gpool
{
    unsigned inst_num;
    struct glyphslot *inst_list;

    struct list_head cache_list;    // The "no reference" slots,
                                    // are sorted by the last dereference time.
    struct list_head using_list;    // The "have reference" slots
    struct txtrdr_glyph_map search_map;

    pthread_rwlock_t rwlock;

} txtrdr_gpool_t;

void txtrdr_gpool_init(txtrdr_gpool_t *self);
void txtrdr_gpool_destroy(txtrdr_gpool_t *self);

int txtrdr_gpool_setup(txtrdr_gpool_t *self, unsigned cache_num);
unsigned txtrdr_gpool_get_cache_num(const txtrdr_gpool_t *self);
int txtrdr_gpool_clear(txtrdr_gpool_t *self);

const struct txtrdr_glyph* txtrdr_gpool_find(
    const txtrdr_gpool_t *self,
    unsigned long long code,
    int height,
    uint32_t colour);
const struct txtrdr_glyph* txtrdr_gpool_insert(
    txtrdr_gpool_t *self, const struct txtrdr_glyph *glyph);

#ifdef __cplusplus
}   // extern "C"
#endif

#endif
