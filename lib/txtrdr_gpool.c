#include <assert.h>
#include <stdatomic.h>
#include <stdlib.h>
#include <string.h>
#include "txtrdr_gpool.h"

struct glyphslot
{
    atomic_int refcnt;
    txtrdr_gpool_t *pool;

    struct list_head list_node;
    RB_ENTRY(glyphslot) map_node;

    struct txtrdr_glyph glyph;
};

#define CONTAINER_OF(ptr, type, field) \
    ((type*)( (char*)(ptr) - (char*)(&((type*)0)->field) ))
#define GLYPH_GET_SLOT(glyph) \
    CONTAINER_OF(glyph, struct glyphslot, glyph)

static
int compare_glyphs(const struct glyphslot *l, const struct glyphslot *r)
{
    uint64_t lv =
        ( ( (uint64_t) l->glyph.code << 43 ) & 0xFFFFF80000000000ULL ) |
        ( ( (uint64_t) l->glyph.height << 32 ) & 0x000007FF00000000ULL ) |
        l->glyph.colour;
    uint64_t rv =
        ( ( (uint64_t) r->glyph.code << 43 ) & 0xFFFFF80000000000ULL ) |
        ( ( (uint64_t) r->glyph.height << 32 ) & 0x000007FF00000000ULL ) |
        r->glyph.colour;
    return
        lv < rv ? -1 :
        lv > rv ? 1 :
        0;
}

#ifdef __GNUC__
#   pragma GCC push_options
#   pragma GCC optimize("no-strict-aliasing")
#endif

RB_GENERATE_STATIC(txtrdr_glyph_map, glyphslot, map_node, compare_glyphs);

#ifdef __GNUC__
#   pragma GCC pop_options
#endif

static
void glyphslot_init(struct glyphslot *slot, txtrdr_gpool_t *pool)
{
    memset(slot, 0, sizeof(*slot));

    atomic_init(&slot->refcnt, 0);
    slot->pool = pool;
}

static
void glyphslot_destroy(struct glyphslot *slot)
{
    if(slot->glyph.image)
        uimg_release(slot->glyph.image);
}

static
void glyphslot_clear_glyph(struct glyphslot *slot)
{
    if(slot->glyph.image)
        uimg_release(slot->glyph.image);
    memset(&slot->glyph, 0, sizeof(slot->glyph));
}

static
void glyphslot_copy_glyph(struct glyphslot *slot, const struct txtrdr_glyph *glyph)
{
    slot->glyph = *glyph;
    slot->glyph.image = uimg_shadow(glyph->image);
}

void txtrdr_gpool_init(txtrdr_gpool_t *self)
{
    self->inst_num = 0;
    self->inst_list = NULL;

    INIT_LIST_HEAD(&self->cache_list);
    INIT_LIST_HEAD(&self->using_list);
    RB_INIT(&self->search_map);

    int err = pthread_rwlock_init(&self->rwlock, NULL);
    assert( err == 0 );
    (void) err; // Avoid the unused warning!
}

void txtrdr_gpool_destroy(txtrdr_gpool_t *self)
{
    int err = txtrdr_gpool_clear(self);
    assert( err == 0 );
    (void) err; // Avoid the unused warning!

    pthread_rwlock_destroy(&self->rwlock);
}

int txtrdr_gpool_setup(txtrdr_gpool_t *self, unsigned cache_num)
{
    pthread_rwlock_wrlock(&self->rwlock);

    int err = -1;
    do
    {
        if(self->inst_list) break;
        if(!cache_num) break;

        size_t list_size = cache_num * sizeof(self->inst_list[0]);
        if( !( self->inst_list = malloc(list_size) ) )
            break;

        self->inst_num = cache_num;

        for(unsigned i = 0; i < cache_num; ++i)
        {
            glyphslot_init(&self->inst_list[i], self);
            list_add_tail(&self->inst_list[i].list_node, &self->cache_list);
        }

        err = 0;
    } while(0);

    pthread_rwlock_unlock(&self->rwlock);

    return err;
}

unsigned txtrdr_gpool_get_cache_num(const txtrdr_gpool_t *self)
{
    return self->inst_num;
}

int txtrdr_gpool_clear(txtrdr_gpool_t *self)
{
    if(!self->inst_list) return 0;

    pthread_rwlock_wrlock(&self->rwlock);

    int err = -1;
    do
    {
        if(!list_empty(&self->using_list))
            break;

        for(unsigned i = 0; i < self->inst_num; ++i)
            glyphslot_destroy(&self->inst_list[i]);
        free(self->inst_list);
        self->inst_list = NULL;
        self->inst_num = 0;

        INIT_LIST_HEAD(&self->cache_list);
        INIT_LIST_HEAD(&self->using_list);
        RB_INIT(&self->search_map);

        err = 0;
    } while(0);

    pthread_rwlock_unlock(&self->rwlock);

    return err;
}

const struct txtrdr_glyph* txtrdr_gpool_find(
    const txtrdr_gpool_t *self,
    unsigned long long code,
    int height,
    uint32_t colour)
{
    pthread_rwlock_rdlock(&((txtrdr_gpool_t*)self)->rwlock);

    struct glyphslot *slot = NULL;
    do
    {
        struct glyphslot target =
        {
            .glyph.code = code,
            .glyph.height = height,
            .glyph.colour = colour,
        };

        slot = RB_FIND(
            txtrdr_glyph_map, (struct txtrdr_glyph_map*) &self->search_map, &target);
    } while(0);

    pthread_rwlock_unlock(&((txtrdr_gpool_t*)self)->rwlock);

    return slot ? &slot->glyph : NULL;
}

static
struct glyphslot* request_free_slot(txtrdr_gpool_t *self)
{
    struct glyphslot *slot =
        list_first_entry_or_null(&self->cache_list, struct glyphslot, list_node);

    if( RB_FIND(txtrdr_glyph_map, &self->search_map, slot) == slot )
        RB_REMOVE(txtrdr_glyph_map, &self->search_map, slot);

    glyphslot_clear_glyph(slot);

    return slot;
}

const struct txtrdr_glyph* txtrdr_gpool_insert(
    txtrdr_gpool_t *self, const struct txtrdr_glyph *glyph)
{
    pthread_rwlock_wrlock(&self->rwlock);

    struct glyphslot *slot = NULL;
    do
    {
        if( !( slot = request_free_slot(self) ) )
            break;

        glyphslot_copy_glyph(slot, glyph);

        struct glyphslot *duplicated =
            RB_INSERT(txtrdr_glyph_map, &self->search_map, slot);
        if(duplicated)
            slot = duplicated;

        if( 0 == atomic_load(&slot->refcnt) )
            list_move_tail(&slot->list_node, &self->cache_list);
    } while(0);

    pthread_rwlock_unlock(&self->rwlock);

    return slot ? &slot->glyph : NULL;
}

struct txtrdr_glyph* txtrdr_glyph_shadow(const struct txtrdr_glyph *glyph)
{
    struct glyphslot *slot = GLYPH_GET_SLOT(glyph);
    txtrdr_gpool_t *pool = slot->pool;

    if( 0 == atomic_fetch_add(&slot->refcnt, 1) )
    {
        pthread_rwlock_wrlock(&pool->rwlock);

        list_del(&slot->list_node);
        list_add_tail(&slot->list_node, &pool->using_list);

        pthread_rwlock_unlock(&pool->rwlock);
    }

    return &slot->glyph;
}

void txtrdr_glyph_release(struct txtrdr_glyph *glyph)
{
    struct glyphslot *slot = GLYPH_GET_SLOT(glyph);
    txtrdr_gpool_t *pool = slot->pool;

    if( 1 == atomic_fetch_sub(&slot->refcnt, 1) )
    {
        pthread_rwlock_wrlock(&pool->rwlock);

        list_del(&slot->list_node);
        list_add_tail(&slot->list_node, &pool->cache_list);

        pthread_rwlock_unlock(&pool->rwlock);
    }
}
