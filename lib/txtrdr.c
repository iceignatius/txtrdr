#include <assert.h>
#include <stdlib.h>
#include <glob.h>
#include "bsd/list.h"
#include "fonts.h"
#include "txtrdr_utf8.h"
#include "txtrdr.h"

#ifndef MIN
#   define MIN(a, b) ( (a) < (b) ? (a) : (b) )
#endif

#ifndef MAX
#   define MAX(a, b) ( (a) > (b) ? (a) : (b) )
#endif

#define CEIL_INT(x, n) ( ( (x) + (n) - 1 ) / (n) * (n) )

struct glyphslot
{
    struct list_head node;
    struct txtrdr_glyph *glyph;
};

static
void init_rect(struct uimg_rect *rect)
{
    rect->x = 0;
    rect->y = 0;
    rect->w = 0;
    rect->h = 0;
}

static
struct uimg_rect union_rect(const struct uimg_rect *l, const struct uimg_rect *r)
{
    if( !l->w && !l->h ) return *r;
    if( !r->w && !r->h ) return *l;

    int l_x0 = l->x;
    int l_y0 = l->y;
    int l_x1 = l_x0 + l->w;
    int l_y1 = l_y0 + l->h;

    int r_x0 = r->x;
    int r_y0 = r->y;
    int r_x1 = r_x0 + r->w;
    int r_y1 = r_y0 + r->h;

    int u_x0 = MIN(l_x0, r_x0);
    int u_y0 = MIN(l_y0, r_y0);
    int u_x1 = MAX(l_x1, r_x1);
    int u_y1 = MAX(l_y1, r_y1);

    struct uimg_rect u =
    {
        .x = u_x0,
        .y = u_y0,
        .w = u_x1 - u_x0,
        .h = u_y1 - u_y0,
    };

    return u;
}

static
struct glyphslot* create_glyphslot(const struct txtrdr_glyph *glyph)
{
    struct glyphslot *slot = malloc(sizeof(*slot));

    if(slot)
        slot->glyph = txtrdr_glyph_shadow(glyph);

    return slot;
}

static
void release_glyphslot(struct glyphslot *slot)
{
    txtrdr_glyph_release(slot->glyph);
    free(slot);
}

static
void clear_glyph_list(struct list_head *list)
{
    struct glyphslot *slot, *tmp;
    list_for_each_entry_safe(slot, tmp, list, node)
    {
        release_glyphslot(slot);
    }

    INIT_LIST_HEAD(list);
}

static
int push_back_glyph_list(struct list_head *list, const struct txtrdr_glyph *glyph)
{
    struct glyphslot *slot = create_glyphslot(glyph);
    if(slot)
        list_add_tail(&slot->node, list);

    return slot ? 0 : -1;
}

static
const struct glyphslot* search_glyph_list(
    struct list_head *list, unsigned long long target)
{
    struct glyphslot *slot;
    list_for_each_entry(slot, list, node)
    {
        if( slot->glyph->code == target )
            return slot;
    }

    return NULL;
}

static
unsigned search_count_glyph_list(
    struct list_head *list, unsigned long long target)
{
    unsigned num = 0;

    struct glyphslot *slot;
    list_for_each_entry(slot, list, node)
    {
        if( slot->glyph->code == target )
            ++num;
    }

    return num;
}

static
void search_replace_glyph_list(
    struct list_head *list,
    unsigned long long target,
    const struct txtrdr_glyph *replacement)
{
    struct glyphslot *slot;
    list_for_each_entry(slot, list, node)
    {
        if( slot->glyph->code == target )
        {
            txtrdr_glyph_release(slot->glyph);
            slot->glyph = txtrdr_glyph_shadow(replacement);
        }
    }
}

static
bool extract_glyph_list(
    struct list_head *sublist, struct list_head *srclist, unsigned long long separator)
{
    clear_glyph_list(sublist);

    while(!list_empty(srclist))
    {
        struct glyphslot *slot =
            list_first_entry(srclist, struct glyphslot, node);

        list_del(&slot->node);
        list_add_tail(&slot->node, sublist);

        if( slot->glyph->code == separator )
            break;
    }

    return !list_empty(sublist);
}

void txtrdr_init(
    txtrdr_t *self, uimg_t *canvas, const txtrdr_fonts_t *fonts, unsigned cache_num)
{
    /**
     * @memberof txtrdr
     * @brief Constructor
     *
     * @param self      Object instance
     * @param canvas    The image to be rendered as a canvas.
     *                  This parameter can be NULL to
     *                  make the renderer generate canvas suitable image
     *                  by it self.
     * @param fonts     The fonts object to be shared to use.
     *                  This parameter can be NULL to use
     *                  the internal managed fonts object.
     * @param cache_num The number of glyph cache slot to use.
     *                  This parameter can be ZERO to use the default value.
     *                  This parameter will be ignored if @p fonts is presented.
     */
    self->canvas = uimg_shadow(canvas);
    self->use_user_canvas = !!self->canvas;
    self->fonts = txtrdr_fonts_shadow(fonts);
    self->fonts_cache_num =
        fonts ? txtrdr_fonts_get_cache_num(fonts) :
        cache_num ? cache_num :
        128;

    self->fg_colour = UIMG_COLOUR_WHITE;
    self->bg_colour = UIMG_COLOUR_BLANK;
    self->align = TXTRDR_LEFT;
    self->tabsize = 4;
    self->tabmode = TXTRDR_TABSTOP;

    self->space_width = 0;
    self->glyphs = NULL;
}

void txtrdr_destroy(txtrdr_t *self)
{
    /**
     * @memberof txtrdr
     * @brief Destructor
     *
     * @param self Object instance
     */
    if(self->glyphs)
    {
        clear_glyph_list(self->glyphs);
        free(self->glyphs);
    }

    if(self->fonts)
        txtrdr_fonts_release(self->fonts);

    if(self->canvas)
        uimg_release(self->canvas);
}

const txtrdr_fonts_t* txtrdr_get_fonts(const txtrdr_t *self)
{
    /**
     * @memberof txtrdr
     * @brief Get renderer's fonts holder
     */
    if(!self->fonts)
        ((txtrdr_t*)self)->fonts = txtrdr_fonts_create(self->fonts_cache_num);

    return self->fonts;
}

int txtrdr_add_font(txtrdr_t *self, const char *filename)
{
    /**
     * @memberof txtrdr
     * @brief Load a font file then add it to the fonts list
     *
     * @param self      Object instance
     * @param filename  Name of the font file
     * @return ZERO if success, and others indicates an error occurred!
     */
    if( !self->fonts &&
        !( self->fonts = txtrdr_fonts_create(self->fonts_cache_num) ) )
    {
        return -1;
    }

    return txtrdr_fonts_add_font(self->fonts, filename);
}

unsigned txtrdr_add_fonts(txtrdr_t *self, const char *wildcard)
{
    /**
     * @memberof txtrdr
     * @brief Load and add font files by wildcard
     *
     * @param self      Object instance
     * @param wildcard  Name of font files described with wildcards
     * @return Count of files which are successfully loaded
     */
    unsigned count = 0;

    glob_t info;
    if(glob(wildcard, 0, NULL, &info))
        return count;

    for(size_t i = 0; i < info.gl_pathc; ++i)
    {
        if( 0 == txtrdr_add_font(self, info.gl_pathv[i]) )
            ++count;
    }

    globfree(&info);

    return count;
}

void txtrdr_clear_fonts(txtrdr_t *self)
{
    /**
     * @memberof txtrdr
     * @brief Clear all loaded fonts
     */
    if(self->fonts)
        txtrdr_fonts_clear(self->fonts);
}

int txtrdr_set_font_size(txtrdr_t *self, unsigned height)
{
    /**
     * @memberof txtrdr
     * @brief Set font size
     *
     * @param self      Object instance.
     * @param height    The desired font height.
     * @return ZERO if success, and others indicates an error occurred!
     *
     * @remarks
     * Change font size during the text input operation
     * may cause the undefined result!
     */
    if( !self->fonts &&
        !( self->fonts = txtrdr_fonts_create(self->fonts_cache_num) ) )
    {
        return -1;
    }

    return txtrdr_fonts_set_size(self->fonts, height);
}

unsigned txtrdr_get_font_size(const txtrdr_t *self)
{
    /**
     * @memberof txtrdr
     * @brief Get current font height
     *
     * @param self Object instance
     * @return Current font height
     */
    return self->fonts ? txtrdr_fonts_get_size(self->fonts) : 0;
}

void txtrdr_set_fg_colour(txtrdr_t *self, uint32_t colour)
{
    /**
     * @memberof txtrdr
     * @brief Set foreground colour
     *
     * @param self      Object instance
     * @param colour    The new foreground colour
     */
    self->fg_colour = colour;
}

void txtrdr_set_bg_colour(txtrdr_t *self, uint32_t colour)
{
    /**
     * @memberof txtrdr
     * @brief Set background colour
     *
     * @param self      Object instance
     * @param colour    The new background colour
     */
    self->bg_colour = colour;
}

void txtrdr_set_align(txtrdr_t *self, int align)
{
    /**
     * @memberof txtrdr
     * @brief Set text alignment mode
     *
     * @param self  Object instance
     * @param align The new alignment mode defined in ::txtrdr_align
     *
     * @remarks The default alignment mode is TXTRDR_ALIGN_LEFT
     */
    switch(align)
    {
    case TXTRDR_LEFT:
    case TXTRDR_RIGHT:
    case TXTRDR_CENTRE:
        self->align = align;
        break;
    }
}

void txtrdr_set_tab_size(txtrdr_t *self, int tabsize)
{
    /**
     * @memberof txtrdr
     * @brief Set tab size
     *
     * @param self    Object instance
     * @param tabsize The new tab size in count of characters
     *                If the font is not width fixed font, then
     *                the actual tab distance will be calculate based on
     *                white spaces (code point 0x20)
     *
     * @remarks The default tab size is 4
     */
    self->tabsize = tabsize;
}

void txtrdr_set_tab_mode(txtrdr_t *self, int tabmod)
{
    /**
     * @memberof txtrdr
     * @brief Set tab mode
     *
     * @param self   Object instance
     * @param tabmod The new tab mode defined in ::txtrdr_tabmod
     *
     * @remarks The default tab mode is TXTRDR_TABMOD_TABSTOP
     */
    switch(tabmod)
    {
    case TXTRDR_TAB_AS_SPACE:
    case TXTRDR_TABSTOP:
    case TXTRDR_TAB_LEFT_RIGHT:
    case TXTRDR_TAB_COLUMNS:
        self->tabmode = tabmod;
        break;
    }
}

uint32_t txtrdr_get_fg_colour(const txtrdr_t *self)
{
    /**
     * @memberof txtrdr
     * @brief Get the current foreground colour
     *
     * @param self Object instance
     * @return The current foreground colour
     */
    return self->fg_colour;
}

uint32_t txtrdr_get_bg_colour(const txtrdr_t *self)
{
    /**
     * @memberof txtrdr
     * @brief Get the current background colour
     *
     * @param self Object instance
     * @return The current background colour
     */
    return self->bg_colour;
}

int txtrdr_get_align(const txtrdr_t *self)
{
    /**
     * @memberof txtrdr
     * @brief Get the current text alignment mode
     *
     * @param self Object instance
     * @return The current alignment mode defined in ::txtrdr_align
     */
    return self->align;
}

int txtrdr_get_tab_size(const txtrdr_t *self)
{
    /**
     * @memberof txtrdr
     * @brief Get the current tab size
     *
     * @param self Object instance
     * @return The current tab size in count of characters
     */
    return self->tabsize;
}

int txtrdr_get_tab_mode(const txtrdr_t *self)
{
    /**
     * @memberof txtrdr
     * @brief Get the current tab mode
     *
     * @param self Object instance
     * @return The current tab mode defined in ::txtrdr_tabmod
     */
    return self->tabmode;
}

int txtrdr_begin_render(txtrdr_t *self, const struct uimg_rect *rect)
{
    /**
     * @memberof txtrdr
     * @brief Prepare to receive text input
     *
     * @param self Object instance.
     * @param rect Area of the canvas to layout text.
     *             This parameter can be NULL, and the area will be equal
     *             to the whole canvas.
     * @return ZERO if success, and others indicates an error occurred!
     */
    if(txtrdr_in_rendering(self)) return -1;
    if(!self->fonts) return -1;

    const struct txtrdr_glyph *space_glyph =
        txtrdr_fonts_get_glyph(self->fonts, ' ', self->fg_colour);
    if(!space_glyph)
        return -1;

    if(!self->glyphs)
    {
        if(( self->glyphs = malloc(sizeof(*self->glyphs)) ))
            INIT_LIST_HEAD(self->glyphs);
        else
            return -1;
    }

    clear_glyph_list(self->glyphs);

    if(!self->use_user_canvas)
    {
        init_rect(&self->draw_rect);
    }
    else if(rect)
    {
        self->draw_rect = *rect;
    }
    else
    {
        self->draw_rect.x = 0;
        self->draw_rect.y = 0;
        self->draw_rect.w = uimg_get_width(self->canvas);
        self->draw_rect.h = uimg_get_height(self->canvas);
    }

    init_rect(&self->text_rect);

    self->space_width = space_glyph->advance;

    return 0;
}

static
void calc_block_size(
    unsigned font_height,
    unsigned font_ascender,
    const struct list_head *glyphlist,
    unsigned *blk_height,
    unsigned *blk_ascender,
    unsigned *blk_width)
{
    int ascender = 0, descender = 0, width = 0;
    const struct glyphslot *slot;
    list_for_each_entry(slot, glyphlist, node)
    {
        int glyph_ascender = slot->glyph->bearing_y;
        if( ascender < glyph_ascender )
            ascender = glyph_ascender;

        int glyph_descender = slot->glyph->area.h - slot->glyph->bearing_y;
        if( descender < glyph_descender )
            descender = glyph_descender;

        int glyph_width = slot->glyph->advance;
        if( glyph_width > 0 )
            width += glyph_width;
    }

    unsigned height = ascender + descender;
    if(blk_height)
        *blk_height = font_height > height ? font_height : height;

    if(blk_ascender)
        *blk_ascender = font_ascender > ascender ? font_ascender : ascender;

    if(blk_width)
        *blk_width = width;
}

static
int render_block(
    uimg_t *canvas,
    unsigned height,
    unsigned ascender,
    const struct list_head *glyphlist,
    const struct uimg_rect *draw_rect,
    struct uimg_rect *text_rect)
{
    if( draw_rect->h < height ) return 0;

    struct uimg_rect curr_rect = *draw_rect;

    text_rect->x = draw_rect->x;
    text_rect->y = draw_rect->y;
    text_rect->w = 0;
    text_rect->h = height;

    struct glyphslot *slot;
    list_for_each_entry(slot, glyphlist, node)
    {
        if( curr_rect.w < slot->glyph->advance ) break;

        if( slot->glyph->area.w && slot->glyph->area.h )
        {
            int dst_x = curr_rect.x + slot->glyph->bearing_x;
            int dst_y = curr_rect.y + ascender - slot->glyph->bearing_y;

            int err = uimgblit_blend(
                canvas,
                dst_x,
                dst_y,
                slot->glyph->image,
                &slot->glyph->area,
                UIMG_BLEND_ALPHA);
            if(err)
                return err;
        }

        if( slot->glyph->advance > 0 )
        {
            curr_rect.x += slot->glyph->advance;
            curr_rect.w -= slot->glyph->advance;
            text_rect->w += slot->glyph->advance;
        }
    }

    return 0;
}

static
int render_column(
    txtrdr_t *self,
    int align,
    const struct list_head *glyphlist,
    const struct uimg_rect *draw_rect,
    struct uimg_rect *text_rect)
{
    unsigned font_height = txtrdr_fonts_get_size(self->fonts);
    unsigned font_ascender = txtrdr_fonts_get_ascender(self->fonts);

    unsigned blk_width, blk_height, blk_ascender;
    calc_block_size(
        font_height,
        font_ascender,
        glyphlist,
        &blk_height,
        &blk_ascender,
        &blk_width);
    if( blk_width > draw_rect->w )
        blk_width = draw_rect->w;

    int blk_posx = 0;
    switch(align)
    {
    case TXTRDR_LEFT:
        blk_posx = draw_rect->x;
        break;

    case TXTRDR_RIGHT:
        blk_posx = draw_rect->x + draw_rect->w - blk_width;
        break;

    case TXTRDR_CENTRE:
        blk_posx = draw_rect->x + ( ( draw_rect->w - blk_width ) >> 1 );
        break;

    default:
        return -1;
    }

    struct uimg_rect blk_rect =
    {
        .x = blk_posx,
        .y = draw_rect->y,
        .w = blk_width,
        .h = draw_rect->h,
    };

    return render_block(
        self->canvas, blk_height, blk_ascender, glyphlist, &blk_rect, text_rect);
}

static
int render_glyph_line_in_tab_as_space(
    txtrdr_t *self, struct list_head *line, struct uimg_rect *line_rect)
{
    const struct txtrdr_glyph *space =
        txtrdr_fonts_get_glyph(self->fonts, ' ', self->fg_colour);
    if(!space)
        return -1;

    search_replace_glyph_list(line, '\t', space);

    return render_column(self, self->align, line, &self->draw_rect, line_rect);
}

static
int render_glyph_line_in_tabstop(
    txtrdr_t *self, struct list_head *line, struct uimg_rect *line_rect)
{
    // Tab stop can work in left-align mode only!
    if( self->align != TXTRDR_LEFT )
        return render_glyph_line_in_tab_as_space(self, line, line_rect);

    struct uimg_rect line_draw_rect = self->draw_rect;
    struct uimg_rect *line_text_rect = line_rect;
    init_rect(line_text_rect);

    unsigned tab_width = self->tabsize * self->space_width;
    int err = 0;

    struct list_head column = LIST_HEAD_INIT(column);
    while(extract_glyph_list(&column, line, '\t'))
    {
        struct uimg_rect col_rect;
        if(( err = render_column(
            self, self->align, &column, &line_draw_rect, &col_rect) ))
        {
            break;
        }

        *line_text_rect = union_rect(line_text_rect, &col_rect);

        unsigned col_width = CEIL_INT(col_rect.w + self->space_width, tab_width);

        line_draw_rect.x += col_width;
        line_draw_rect.w -= col_width;
        if( line_draw_rect.w < 0 )
            break;
    }

    clear_glyph_list(&column);

    return err;
}

static
int render_glyph_line_in_tab_left_right(
    txtrdr_t *self, struct list_head *line, struct uimg_rect *line_rect)
{
    const struct glyphslot *separator = search_glyph_list(line, '\t');
    if(!separator)
        return render_column(self, self->align, line, &self->draw_rect, line_rect);

    const struct txtrdr_glyph *space =
        txtrdr_fonts_get_glyph(self->fonts, ' ', self->fg_colour);
    if(!space)
        return -1;

    struct list_head left_column = LIST_HEAD_INIT(left_column);
    extract_glyph_list(&left_column, line, '\t');

    struct list_head right_column = LIST_HEAD_INIT(right_column);
    extract_glyph_list(&right_column, line, '\n');
    search_replace_glyph_list(&right_column, '\t', space);

    int err = -1;
    do
    {
        struct uimg_rect left_rect;
        if(( err = render_column(
            self, TXTRDR_LEFT, &left_column, &self->draw_rect, &left_rect) ))
        {
             break;
        }

        struct uimg_rect right_rect;
        if(( err = render_column(
            self, TXTRDR_RIGHT, &right_column, &self->draw_rect, &right_rect) ))
        {
            break;
        }

        *line_rect = union_rect(&left_rect, &right_rect);

        err = 0;
    } while(false);

    clear_glyph_list(&left_column);
    clear_glyph_list(&right_column);

    return err;
}

static
int render_glyph_line_in_tab_columns(
    txtrdr_t *self, struct list_head *line, struct uimg_rect *line_rect)
{
    unsigned col_num = 1 + search_count_glyph_list(line, '\t');
    unsigned col_width = self->draw_rect.w / col_num;
    if(!col_width) return 0;

    init_rect(line_rect);
    int err = 0;

    struct list_head column = LIST_HEAD_INIT(column);
    for(int col_index = 0; extract_glyph_list(&column, line, '\t'); ++col_index)
    {
        struct uimg_rect col_draw_rect =
        {
            .x = self->draw_rect.x + col_index * col_width,
            .y = self->draw_rect.y,
            .w = col_width,
            .h = self->draw_rect.h,
        };

        struct uimg_rect col_text_rect;
        if(( err = render_column(
            self, self->align, &column, &col_draw_rect, &col_text_rect) ))
        {
            break;
        }

        *line_rect = union_rect(line_rect, &col_text_rect);
    }

    clear_glyph_list(&column);

    return err;
}

static
int render_glyph_line(
    txtrdr_t *self, struct list_head *line, struct uimg_rect *line_rect)
{
    switch(self->tabmode)
    {
    case TXTRDR_TAB_AS_SPACE:
        return render_glyph_line_in_tab_as_space(self, line, line_rect);

    case TXTRDR_TABSTOP:
        return render_glyph_line_in_tabstop(self, line, line_rect);

    case TXTRDR_TAB_LEFT_RIGHT:
        return render_glyph_line_in_tab_left_right(self, line, line_rect);

    case TXTRDR_TAB_COLUMNS:
        return render_glyph_line_in_tab_columns(self, line, line_rect);
    }

    return -1;
}

static
void calc_text_render_size(
    unsigned int font_height,
    unsigned int font_ascender,
    struct list_head *glyphlist,
    struct uimg_rect *rect)
{
    init_rect(rect);

    struct list_head proced = LIST_HEAD_INIT(proced);

    struct list_head line = LIST_HEAD_INIT(line);
    while(extract_glyph_list(&line, glyphlist, '\n'))
    {
        unsigned height, ascender, width;
        calc_block_size(
            font_height, font_ascender, &line, &height, &ascender, &width);

        rect->w = rect->w >= width ? rect->w : width;
        rect->h += height;

        list_splice_tail_init(&line, &proced);
    }

    assert(list_empty(&line));
    clear_glyph_list(&line);

    assert(list_empty(glyphlist));
    list_splice_tail_init(&proced, glyphlist);
}

int txtrdr_end_render(txtrdr_t *self, struct uimg_rect *rect)
{
    /**
     * @memberof txtrdr
     * @brief Finish text input works
     *
     * @param self Object instance.
     * @param rect Returns the minimal area that have text rendered.
     *             This parameter can be NULL to ignore the area.
     * @return ZERO if success, and others indicates an error occurred!
     */
    if(!txtrdr_in_rendering(self))
        return -1;

    if(!self->use_user_canvas)
    {
        const struct txtrdr_glyph *space =
            txtrdr_fonts_get_glyph(self->fonts, ' ', self->fg_colour);
        if(!space)
            return -1;

        search_replace_glyph_list(self->glyphs, '\t', space);

        struct uimg_rect rect;
        calc_text_render_size(
            txtrdr_fonts_get_size(self->fonts),
            txtrdr_fonts_get_ascender(self->fonts),
            self->glyphs,
            &rect);

        // Monopolise the image buffer
        if( self->canvas && 1 < uimg_get_refcnt(self->canvas) )
        {
            uimg_release(self->canvas);
            self->canvas = NULL;
        }

        if( !self->canvas && !( self->canvas = uimg_create(rect.w, rect.h) ) )
            return -1;
        if(uimg_resize(self->canvas, rect.w, rect.h))
            return -1;

        self->draw_rect = rect;
    }

    uimgblit_fill(self->canvas, self->bg_colour, &self->draw_rect);

    struct list_head line = LIST_HEAD_INIT(line);
    while(extract_glyph_list(&line, self->glyphs, '\n'))
    {
        struct uimg_rect line_rect;
        if(render_glyph_line(self, &line, &line_rect))
            break;

        self->draw_rect.y += line_rect.h;
        self->draw_rect.h -= line_rect.h;
        self->text_rect = union_rect(&self->text_rect, &line_rect);
    }

    clear_glyph_list(&line);

    if(rect)
        *rect = self->text_rect;

    self->space_width = 0;

    return 0;
}

bool txtrdr_in_rendering(const txtrdr_t *self)
{
    /**
     * @memberof txtrdr
     * @brief Check if in text input state
     *
     * @param self Object instance
     * @return TRUE if in text input state, and FALSE if not
     */
    return self->space_width > 0;
}

static
int read_glyphs_from_text(
    struct list_head *list, const char *text, txtrdr_fonts_t *fonts, uint32_t colour)
{
    unsigned long long code;
    while(( text = txtrdr_utf8_decode(text, &code) ))
    {
        const struct txtrdr_glyph *glyph =
            txtrdr_fonts_get_glyph(fonts, code, colour);
        if( glyph && push_back_glyph_list(list, glyph) )
            return -1;
    }

    return 0;
}

int txtrdr_push_text(txtrdr_t *self, const char *text)
{
    /**
     * @memberof txtrdr
     * @brief Add (more) text to be rendered
     *
     * @param self Object instance.
     * @param text The text to be input.
     * @return ZERO if success, or others indicates an error occurred!
     *
     * @remarks The text can be input only if
     *          the render has begin a text input work,
     *          otherwise, the function will failed!
     */
    if(!txtrdr_in_rendering(self)) return -1;
    if(!self->glyphs) return -1;

    return text ?
        read_glyphs_from_text(self->glyphs, text, self->fonts, self->fg_colour) :
        0;
}

const uimg_t* txtrdr_get_image(const txtrdr_t *self)
{
    /**
     * @memberof txtrdr
     * @brief Get the canvas image
     */
    return self->canvas;
}

const uimg_t* txtrdr_quick_gen(txtrdr_t *self, const char *text)
{
    /**
     * @memberof txtrdr
     * @brief Help to generate rendered image quickly
     * @details Calling of this function is equivalent to call
     *          txtrdr_begin_render(), txtrdr_push_text(), txtrdr_end_render(),
     *          and txtrdr_get_image().
     *
     * @param self Object instance
     * @param text The text to be rendered
     * @return The rendered image if success; or NULL on error occurred.
     */
    bool succ = false;
    do
    {
        if(txtrdr_begin_render(self, NULL))
            break;

        if(txtrdr_push_text(self, text))
            break;

        if(txtrdr_end_render(self, NULL))
            break;

        succ = true;
    } while(false);

    if(txtrdr_in_rendering(self))
        txtrdr_end_render(self, NULL);

    return succ ? txtrdr_get_image(self) : NULL;
}
