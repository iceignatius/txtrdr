#include <assert.h>
#include <stdatomic.h>
#include <math.h>
#include <ft2build.h>
#include FT_FREETYPE_H
#include "txtrdr_ttfont.h"

// Convert fixed point 26.6 value to an integer value.
#define FIXPT266_to_INT(val) ( (val) >> 6 )

static FT_Library lib_inst;
static atomic_int lib_refcnt = ATOMIC_VAR_INIT(0);

static int ttfont_open(txtrdr_ttfont_t *self, const char *filename, unsigned height);
static void ttfont_close(txtrdr_ttfont_t *self);

static
void ttfont_init(txtrdr_ttfont_t *self)
{
    self->lib_ref_added = false;
    self->face = NULL;

    self->font_height = 0;
    self->ascender = 0;
    self->descender = 0;
}

static
void ttfont_destroy(txtrdr_ttfont_t *self)
{
    ttfont_close(self);
}

static
int ttfont_open(txtrdr_ttfont_t *self, const char *filename, unsigned height)
{
    ttfont_close(self);

    bool succ = false;
    do
    {
        if(!filename) break;

        bool need_init_lib = ( 0 == atomic_fetch_add(&lib_refcnt, 1) );
        self->lib_ref_added = true;

        if( need_init_lib && FT_Init_FreeType(&lib_inst) )
            break;

        if(FT_New_Face(lib_inst, filename, 0, &self->face))
        {
            self->face = NULL;
            break;
        }

        if(txtrdr_ttfont_set_font_size(self, height))
            break;

        succ = true;
    } while(false);

    if(!succ)
        ttfont_close(self);

    return succ ? 0 : -1;
}

static
void ttfont_close(txtrdr_ttfont_t *self)
{
    self->font_height = 0;
    self->ascender    = 0;
    self->descender   = 0;

    if(self->face)
    {
        FT_Done_Face(self->face);
        self->face = NULL;
    }

    bool need_quit_lib = false;
    if(self->lib_ref_added)
    {
        need_quit_lib = ( 1 == atomic_fetch_sub(&lib_refcnt, 1) );
        self->lib_ref_added = false;
    }

    if(need_quit_lib)
        FT_Done_FreeType(lib_inst);
}

txtrdr_ttfont_t* txtrdr_ttfont_create_open(const char *filename, unsigned height)
{
    txtrdr_ttfont_t *inst = NULL;

    bool succ = false;
    do
    {
        if( !( inst = malloc(sizeof(*inst)) ) )
            break;

        ttfont_init(inst);

        if(ttfont_open(inst, filename, height))
            break;

        succ = true;
    } while(false);

    if( !succ && inst )
    {
        ttfont_destroy(inst);
        free(inst);
        inst = NULL;
    }

    return inst;
}

void txtrdr_ttfont_release(txtrdr_ttfont_t *self)
{
    ttfont_destroy(self);
    free(self);
}

int txtrdr_ttfont_set_font_size(txtrdr_ttfont_t *self, unsigned height)
{
    if(!self->face)
        return -1;

    static const unsigned width = 0;
    if(FT_Set_Pixel_Sizes(self->face, width, height))
        return -1;

    self->font_height = height;

    unsigned ascender_raw  = abs(self->face->ascender);
    unsigned descender_raw = abs(self->face->descender);
    self->ascender =
        round( ( height * ascender_raw ) /
        (double)( ascender_raw + descender_raw ) );
    self->descender = height - self->ascender;

    return 0;
}

unsigned txtrdr_ttfont_get_font_size(txtrdr_ttfont_t *self)
{
    return self->font_height;
}

unsigned txtrdr_ttfont_get_font_ascender(txtrdr_ttfont_t *self)
{
    return self->ascender;
}

static
uimg_t* create_image_from_ft_bitmap(FT_Bitmap *bitmap, uint32_t colour)
{
    assert( bitmap->pixel_mode == FT_PIXEL_MODE_GRAY );

    unsigned w = bitmap->width;
    unsigned h = bitmap->rows;

    unsigned b = UIMG_COLOUR_GET_B(colour);
    unsigned g = UIMG_COLOUR_GET_G(colour);
    unsigned r = UIMG_COLOUR_GET_R(colour);
    unsigned a = UIMG_COLOUR_GET_A(colour);

    uimg_t *image = uimg_create(w, h);
    if(!image) return NULL;

    uimg_begin_buf_access(image);
    for(unsigned y = 0; y < h; ++y)
    {
        const uint8_t *srcrow = &bitmap->buffer[ y * bitmap->pitch ];
        uint32_t *dstrow = uimg_get_row_buf(image, y);

        for(unsigned x = 0; x < w; ++x)
        {
            unsigned s = srcrow[x] + 1;
            dstrow[x] = UIMG_MAKE_COLOUR(
                ( s * b ) >> 8,
                ( s * g ) >> 8,
                ( s * r ) >> 8,
                ( s * a ) >> 8);
        }
    }
    uimg_end_buf_access(image);

    return image;
}

static
void fill_normal_glyph_info(
    struct txtrdr_glyph *glyph,
    unsigned long long code,
    int height,
    uint32_t colour,
    FT_GlyphSlot ftinfo,
    const uimg_t *image)
{
    glyph->code = code;
    glyph->type = TXTRDR_GLYPH_NORMAL;

    glyph->height = height;
    glyph->advance = FIXPT266_to_INT(ftinfo->metrics.horiAdvance);
    glyph->bearing_x = ftinfo->bitmap_left;
    glyph->bearing_y = ftinfo->bitmap_top;
    glyph->colour = colour;

    glyph->image = uimg_shadow(image);
    glyph->area.x = 0;
    glyph->area.y = 0;
    glyph->area.w = uimg_get_width(image);
    glyph->area.h = uimg_get_height(image);
}

int txtrdr_ttfont_get_glyph(
    txtrdr_ttfont_t *self,
    unsigned long long code,
    uint32_t colour,
    struct txtrdr_glyph *glyph)
{
    unsigned index = FT_Get_Char_Index(self->face, code);
    if(!index) return -1;

    uimg_t *image = NULL;

    int succ = false;
    do
    {
        if(FT_Load_Glyph(self->face, index, 0))
            break;
        if(FT_Render_Glyph(self->face->glyph, FT_RENDER_MODE_NORMAL))
            break;

        if( !( image = create_image_from_ft_bitmap(&self->face->glyph->bitmap, colour) ) )
            break;

        fill_normal_glyph_info(
            glyph, code, self->font_height, colour, self->face->glyph, image);

        succ = true;
    } while(false);

    if(image)
        uimg_release(image);

    return succ ? 0 : -1;
}
